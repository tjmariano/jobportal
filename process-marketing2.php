<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";

$loc = get_home();

$curr_date = date('Y-m-d H:i:s');

$query = "SELECT set_content, set_attachment FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ003'";
$set_data = $database->query($query);

$query = "SELECT seta_id, seta_first_name, seta_last_name, seta_email FROM navy_set_answer WHERE seta_token = "."'".$_POST['id']."'";

$user_data = $database->query($query);


if(isset($_POST['submit_answers'])){

	$answers = array();
	$questions = json_decode($set_data[0]['set_content'],true);
	$i = 1;
		foreach($questions as $question){
			$question_id = 'FPZ003-'.$question['order'];
			if($question["type"] === "essay"){
			$temp = array($question['order'],$_POST[$question_id]);
			array_push($answers,$temp);
			//$answers["$question['order']"] = $_POST[$question_id];
			}
			elseif($question["type"] === "file"){
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES[$question_id]["name"]);
				$extension = end($temp);
				if (($_FILES[$question_id]["type"] == "application/pdf")&&($_FILES[$question_id]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES[$question_id]["error"] > 0){
						echo "Return Code: " . $_FILES[$question_id]["error"] . "<br>";
					}else{
					/*echo "Uploaded: " . "$root/uploads/round2answers/" .$user_data[0]['seta_id']."-q".$i.".pdf" . "<br>";*/
					move_uploaded_file($_FILES[$question_id]["tmp_name"], "$root/uploads/round2answers/" .$user_data[0]['seta_id']."-q".$i.".pdf");
					$i++;
					}
				}

			}
		}
		
		
		$answers_obj = array(
			'set3_set'			=> 'FPZ003',
			'set3_answer'		=> json_encode($answers),
			'set3_date_taken'   => $curr_date,
			'set3_seta_id'      => $user_data[0]['seta_id']
		);	
		
		if($database->insertRow('navy_set3_answer', $answers_obj)){
			$loc = get_thankYou2Page();
		}
		else{
			$loc = get_errorPage();
			}
		
	
}

$to = $user_data[0]['seta_email'];
$subject ="FourPoint.Zero, Inc. - Marketing Executive Application Update";

$message = "
<html>
<head>
<title>FourPoint.Zero Application</title>
</head>
<body>
<p>Hi ".$user_data[0]['seta_first_name']."!</p>
<p>Thank you for your time to answer our second set of questions. Please expect an email or phone call from us in a day or two regarding your application.<p>
<br/>
<p>Regards,<p>
<p>FourPoint.Zero, Inc.<p>
<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

mail($to,$subject,$message,$headers);

header("Location: $loc");


$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
$subject2 ="Marketing Executive Application (Round 2) - ".$user_data[0]['seta_first_name']." ".$user_data[0]['seta_last_name'];

$message = "
<html>
<head>
<title>FourPoint.Zero Applicant</title>
</head>
<body>
<p>Hi Admin!</p><br>
<p>".$user_data[0]['seta_first_name']." ".$user_data[0]['seta_last_name']." has completed the second set of answers. For more info, Please click the link below.<p>
<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

<p>Regards<p>
<p>FPZ Recruitment<p>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

mail($to2,$subject2,$message,$headers);

header("Location: $loc");

exit;

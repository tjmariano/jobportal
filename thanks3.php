<?php 
require_once('ignition.php');

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
$set_data = $database->query($query);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	

    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">

      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">You. Are. Awesome.</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
       
          <h2 class="centered" style = "font-size:30px;">Thanks for completing the final stage of our recruitment process.</h2><br>
          <p align = "center" style = "font-size:20px;">Please expect a phone call or an email regarding your application.</p>
    
      </div>



      <?php 
require_once("footer-front.php");
      ?>

	
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	
  </body>
</html>
s
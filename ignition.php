<?php

include 'includes/eden.php';
eden()->routeClasses(true)->routeMethods(true);
eden()->setLoader(true);

date_default_timezone_set('Asia/Manila');
$curr_date = date('Y-m-d');
$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'root', ''); //instantiate database object

//tamayojp_db


function get_home(){
	return "http://jobs.fourptzero.com/";
}

function get_admin(){
	return "http://jobs.fourptzero.com/manage/index.php";
}
function get_answers(){
	return "http://jobs.fourptzero.com/manage/answers.php";
}
function get_login(){
	return "http://jobs.fourptzero.com/login.php";
}

function get_thankYouPage(){
	return "http://jobs.fourptzero.com/thanks1.php";
}
function get_thankYou2Page(){
	return "http://jobs.fourptzero.com/thanks2.php";
}
function get_thankYou3Page(){
	return "http://jobs.fourptzero.com/thanks3.php";
}
function get_errorPage(){
	return "http://jobs.fourptzero.com/error.php";
}
function get_errorFilePage(){
	return "http://jobs.fourptzero.com/error-file.php";
}
function get_testsorry(){
	return "http://jobs.fourptzero.com/sorry-test.php";
}


function check_login($currSession){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');
	$query = "SELECT * FROM navy_users where user_username = '".$currSession."' AND user_company = 1 AND user_active = 1";
	$data = $database->query($query);

	$count_data = count($data);

	if($count_data == 1){

	}
	else{
		header( "Location:".get_login());
		exit;
	}
}
function check_status($status){
	if((strcmp($status, 'forR2')!== 0)){
 header( "Location:".get_errorPage());
		exit;
	}
}
function check_status2($status){
	if((strcmp($status, 'forR3')!== 0)){
 header( "Location:".get_errorPage());
		exit;
	}
}
function supply_questionFields($question,$set){
	echo "<p class='text-muted'>".$question['question']."</p>";
	if($question["type"] === "essay"){

		echo "<textarea name='$set-".$question['order']."' class='form-control' rows='4' ></textarea><br/>";
	}
	elseif($question["type"] === "file"){

		echo "<input type = 'file' name='$set-".$question['order']."' id='$set-".$question['order']."' class='form-control' accept = 'application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document' required/><br/>
		<p class = 'justified' style ='color:red;''><b><i>IMPORTANT REMINDER! Pls Upload PDF, DOC, OR DOCX files only.</i></b></p>";
	}
	elseif($question["type"] === "link"){
		echo
		"<p class = 'justified' style ='color:red;'><b>Insert your video link below.</b></p>
		<div class='form-group input-group full-width'>
                  <input type='url' class='form-control' placeholder='Insert video link here' name='$set-".$question['order']."' required title = '(ex: http://www.youtube.com/)'/>
        </div>";
	}
	elseif($question["type"] === "image"){

		echo "<input type = 'file' name='$set-".$question['order']."' id='$set-".$question['order']."' class='form-control' accept = '.png' required/><br/>
		<p class = 'justified' style ='color:red;''><b><i>IMPORTANT REMINDER! Pls Upload PNG files only.</i></b></p>";
	}
	elseif($question["type"] === "field"){

		echo "<input type = 'text' name='$set-".$question['order']."' class='form-control' placeholder = 'Insert password here (if any)'/><br/>";
	}
}

function show_positionButtons($positions){
	foreach($positions as $position){
		echo "
			<p class = 'centered'><a class='btn btn-default' style = 'width: 70%;' href='".get_home()."position/".$position['set_type_slug']."'>".$position['set_type_desc']."</a></p>
		";
	}
}

function add_uploadEncType($bool){
	if($bool === "YES"){
		echo 'enctype="multipart/form-data"';
	}
}

function send_applicantNotification($object){
	$to = $object[0];
	$subject ="FourPoint.Zero, Inc. - ".$object[1]." Application";

	$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$object[2]."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.</p>
		<p>If you do not receive an email please do contact us at jobs@fourptzero.com</p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
	";

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function send_applicantNotificationUpdate($object){
	$to = $object[0];
	$subject ="FourPoint.Zero, Inc. - ".$object[1]." Application Update";

	$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$object[2]."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.</p>
		<p>If you do not receive an email please do contact us at jobs@fourptzero.com</p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
	";

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}


function send_adminNotification($object){
	//$to= "zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, sebastian.mapy@gmail.com, , marlon@fourptzero.com, jobs@fourptzero.com";
	$to= $object[4];
	$subject = $object[0]." Application (Round ".$object[1].") - ".$object[2]." ".$object[3];

	$title =
		"<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>";

	if($object[1] === '1'){
	$body =	
		"<body>
		<p>Hi ".$object[5]."!</p><br>
		<p>".$object[2]." ".$object[3]." is applying as ".$object[0].".</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>";
		}
	else{
	$body =	
		"<body>
		<p>Hi ".$object[5]."!</p><br>
		<p>".$object[2]." ".$object[3]." applying as ".$object[0]." has completed Round ".$object[1].".</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>";
		}
	
	$footer =
		"<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>";
	$message = $title . $body . $footer;

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}


function goToError(){
	header("Location: ".get_errorPage());
}
function hashpass($var){
	return md5(md5($var));
}

?>
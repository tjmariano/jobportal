<?php 
require_once('ignition.php');
$id = $_GET['id'];
$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code ='FPZ002'";
$set_data = $database->query($query);


$query = "SELECT seta_first_name, seta_last_name, seta_email, seta_status FROM navy_set_answer WHERE seta_token = "."'".$id."'";
$user_data = $database->query($query);
check_status2($user_data[0]['seta_status']);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
    <link rel="stylesheet" href="bootstrap/formvalidation/css/formValidation.min.css">
    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->

    <div class="internheader">
      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">Wow! Impressive.</h1><br>
        <p class="centered" style="color:#fff;font-size:20px;">One last thing.<br>Good luck!</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns --><p class = "whitespace"></p>
        <h2 class="centered" style="font-size:30px;">You applied as a Marketing Executive.</h2><br>
        <p class = "centered" style="font-size:20px;">Let's do this! <a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#startModal">Fight.</a></p>
      
      </div>

      <hr>

      <footer>
      <p align="center" style = "font-size:15px;">&copy; An Innovation Fridays Project, by <font color="blue"><a href="http://www.fourptzero.com" target="_blank"> FourPoint.Zero, Inc. 2015</a></font></p>
      </footer>
      </footer>
    </div> <!-- /container -->

    <!-- Modal
    ================================================== -->
    
<div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Marketing Executive Mock Interview</h4>
            </div>

            <div class="modal-body">
              <p class = "justified" style ="color:gray;">Before we do the final interview, we want to make sure you are who you say you are. This mock interview will provide us insights and better expectations of how you carry yourself.</p>
                <p class = "justified"><b>Instructions.</b></p>
                <p class = "justified" style ="color:gray;">1. Use your smart phones or webcam to record a quick, video answering the questions below.</p>
                <p class = "justified" style ="color:gray;">2. Upload your video in <a href="http://youtube.com" target ="_blank">Youtube</a> or <a href="http://vimeo.com" target ="_blank">Vimeo</a></p>
                <p class = "justified" style ="color:gray;">3. Send us the link. (along with a password if it's set to private)</p>
                  <form id="marketingInternForm" method="post" action="process-marketing3.php" >
                <div class="form-group input-group full-width">
                  <input type="text" class="form-control" placeholder="Insert video link here (and password if any)" name="youtubeLink" required/>
                </div>
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>"/> 
                    <p class = "justified"><b>Questions.</b></p>
                    <p class = "justified">
                      <ol>
                        <li>What's your story?</li>
                        <li>Who is your role model, and why?</li>
                        <li>What things do you not like to do?</li>
                        <li>Tell me about a project or accomplishment that you consider to be the most significant in your career.</li>
                        <li>We're constantly making things better, faster, smarter or less expensive. We leverage technology or improve processes. In other words, we strive to do more--with less. Tell me about a recent project or problem that you made better, faster, smarter, more efficient, or less expensive.</li>
                        <li>When have you been most satisfied in your life?</li>
                        <li>Tell us about a time when things didn't go the way you wanted.</li>
                        <li>If hired, what would make you leave this company?</li>
                        <li>Discuss a specific accomplishment you've achieved in a previous position that indicates you will thrive in this position.</li>
                        <li>If hired, what have we accomplished together in a year?</li>
                      </ol>
                    </p>
                  <br />
                   <div class="form-group">
                        <button type="submit" name="submit_answers" class="btn btn-default">Submit</button>
                    </div>
                 </form>
            
        </div>
    </div>
</div>
</div>
</div>
  </div>
</div>
</div>
    
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/formvalidation/js/formValidation.min.js"></script>
    <script src="bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
  </body>
</html>

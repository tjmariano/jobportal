<?php 
require_once('ignition.php');
$query = "SELECT set_type_desc, set_type_slug FROM navy_set_type WHERE set_type_company = 1 AND set_type_active = 1 ORDER BY set_type_desc ASC";
$positions = $database->query($query);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	
	<link rel="stylesheet" href="bootstrap/formvalidation/css/formValidation.min.css">
    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="startheader">
      <div class="container">
        <h1 class="centered" style="color:#fff; font-size:60px;">We Are Hiring!</h1>
        <p class="centered" style="color:#fff; font-size:20px;"></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
         <br> <h2 class="centered" style = "font-size:30px;">Jobs at FourPointZero, Inc.</h2><br/>
         <p class="centered" style = "font-size:20px;">We want someone who loves to do great work, who loves to think, who loves new opportunities,<br> and wants to change the world.</p><br/>
         	<p class = "centered" style="font-size:20px;"><a class="btn btn-default samesize2" href="#" role="button" data-toggle="modal"  data-target="#startModal">Openings & Opportunities</a></p>
       </div>
   


     <?php 
require_once("footer-front.php");
      ?> <!-- /container -->

    <!-- Modal
    ================================================== -->
	<div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hello! What position are you applying for?</h4>
            </div>

            <div class="modal-body">
            	<?php 
				show_positionButtons($positions);
				?>
            	</div>
        	</div>
    	</div>
	</div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60320115-1', 'auto');
  ga('send', 'pageview');

</script>

  </body>

</html>


<?php 
require_once('ignition.php');
$id = $_GET['id'];
$query = "SELECT set_content, set_attachment FROM navy_set WHERE set_company = 1 AND set_code ='FPZ003'";
$set_data = $database->query($query);


$query = "SELECT seta_first_name, seta_last_name, seta_email, seta_status FROM navy_set_answer WHERE seta_token = "."'".$id."'";
$user_data = $database->query($query);



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
    <link rel="stylesheet" href="bootstrap/formvalidation/css/formValidation.min.css">
    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="internheader">
      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">So you made it through.</h1><br>
        <p class="centered" style="color:#fff;font-size:20px;">Congratulations for making it this far. <br> At this stage, It only gets tougher.</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns --><p class = "whitespace"></p>
        <h2 class="centered" style="font-size:30px;">You applied as a Java Developer.</h2><br>
        <p class = "centered" style="font-size:20px;"><a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#startModal">Second Round. Fight.</a></p>
      
      </div>

      <hr>

      <footer>
      <p align="center" style = "font-size:15px;">&copy; An Innovation Fridays Project, by <font color="blue"><a href="http://www.fourptzero.com" target="_blank"> FourPoint.Zero, Inc. 2015</a></font></p>
      </footer>
      </footer>
    </div> <!-- /container -->

    <!-- Modal
    ================================================== -->
    
<div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Java Developer Technical Test</h4>
            </div>

            <div class="modal-body">
                <p class = "justified" style ="color:gray;">Thank you for taking time in answering the initial questionnaire. 
                  We hope that you are still interested in working with us, as we are excited in the possibility of seeing you join the team.</p>
                <p class = "justified" style ="color:gray;">We are glad to know that you have the qualities, character, and passion to work with us and thrive in our culture. </p>
                <p class = "justified" style ="color:gray;">We have trimmed down the applicants for this position with the initial questionnaire, 
                  but would like to filter still with this technical test. This will let us assess your creativity, marketing capability, and business savvy. 
                  We appreciate honesty, just let us know if you can’t do any of this.</p>
                  <form id="marketingInternForm" method="post" action="process-javadev2.php" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $_GET['id'];?>"/> 
                    <div class="form-group input-group full-width">
                        <HR>
                          <p class = "justified" style ="color:red;"><b><i>IMPORTANT REMINDER! Pls Upload PDF files only.</i></b></p>
                          <?php
                    $questions = json_decode($set_data[0]['set_content'],true);
                    foreach($questions as $question){
                      supply_field($question);
                  ?>
                    <!--h4 class="section-heading" style = "font-color:blue;">Question #<?php echo $question['order'];?></h4-->
                            
                    <br/>
                    <?php       
                        }
                    ?>
                  <br />
                  <br />
                  <br />
                   <div class="form-group">
                    <br/>
                      <br/>
                        <button type="submit" name="submit_answers" class="btn btn-default">Submit</button>
                    </div>
                 </form>
            </div>
        </div>
    </div>
</div>
</div>
  </div>
</div>
</div>
    
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/formvalidation/js/formValidation.min.js"></script>
    <script src="bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
    
    
  </body>
</html>

<?php 
require_once('ignition.php');

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
$set_data = $database->query($query);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	

    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">

      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">Sorry.</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
       
          <h2 class="centered" style = "font-size:30px;">You have already taken this test.</h2><br>
          <p align = "center" style = "font-size:20px;">Please wait for an e-mail regarding your next step.</p>
    
      </div>

      <hr>

      <footer>
        <p align="center" style = "font-size:15px;">&copy; An Innovation Fridays Project, by <font color="blue"><a href="http://www.fourptzero.com/" target="_blank"> FourPoint.Zero, Inc. 2015</a></font></p>
      </footer>
    </div> <!-- /container -->

	
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

	
  </body>
</html>
<?php 
require_once('ignition.php');

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ004'";
$set_data = $database->query($query);


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
  
  <link rel="stylesheet" href="bootstrap/formvalidation/css/formValidation.min.css">
    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="internheader">
      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">So you wanna join the team.</h1><br>
        <p class="centered" style="color:#fff;font-size:20px;">Part of our core value is efficiency in everything we do. This includes how we add new members to the team.<br/>
        We built this tool to help us efficiently filter the winners from the posers. Shall we start?</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns --><p class = "whitespace"></p>
        <h2 class="centered" style="font-size:30px;">You applied as a Product Manager.</h2><br>
        <p class = "centered" style="font-size:20px;"><a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#startModal">Who we are. What we need.</a></p>
            <center><p><a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#examModal">Let's Start &raquo;</a></p></center>
       </div>
   

      <hr>

     <footer>
        <p align="center" style = "font-size:15px;">&copy; An Innovation Fridays Project, by <font color="blue"><a href="http://www.fourptzero.com" target="_blank"> FourPoint.Zero, Inc. 2015</a></font></p>
      </footer>
    </div> <!-- /container -->

    <!-- Modal
    ================================================== -->
    <div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class = "tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Introduction</a></li>
                <li><a href="#tab2" data-toggle="tab">Requirements</a></li>
              </ul>
          <div class = "tab-content">
          <div class="tab-pane active" id="tab1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">We are FourPoint.Zero. We need a Product Manager.</h4>
            </div>

            <div class="modal-body">
                <p class = "justified"><b>Let us first introduce ourselves.</b></p>
                <p class = "justified" style ="color:gray;">We're a small, energetic team that is building products that actually adds value to a lot of people. 
                    We're small enough to be focused and effective even while we're growing quickly. 
                    We put a lot of effort into making our home base a fun place for working and getting things done; we only hire when the timing and person are right.</p>
                <p class = "justified" style ="color:gray;">FourPoint.Zero is a small technology company, bootstrapped, profitable, and chaotic good.</p>
                <p class = "justified" style ="color:gray;">We want someone who loves to do great work, who loves to think, who loves new opportunities, and who wants to change the world.</p>
                <p class = "justified"><b>Our Culture.</b></p>
                <p class = "justified" style = "color:gray;">FourPoint.Zero’s culture is focused on entrepreneurship. 
                    No matter how good you are when you come in, it is our goal to make every day a day for you to challenge yourself, 
                    find opportunities to create new business, and make yourself some money. 
                    Coding, programming, marketing, show-and-tell — whatever it takes, we want you to do it to make you a success.<br/><br/>
                    We don’t offer competitive salaries, and fancy health benefits… but we offer stock options, partnerships, real world learning experience, 
                    flexible hours, a fun and revitalizing work place, and opportunities for you to follow your entrepreneurial dreams.</p>
                <p class = "justified" ><ul style ="color:gray;">
                                            <li>Work on products that affects people every day.</li>
                                            <li>Work with a bunch of cool people who love doing what they do.</li>
                                            <li>Need a break? Get out of the office, swim, jog, play video games, chill.</li>
                                            <li>Innovation Fridays! Fridays are not normal work days - you can take the half day off, 
                                                work on your own project/product, or work your sidelines.</li>
                                        </ul>
                </p>
                <p class = "justified" style ="color:gray;">Please DO NOT apply if:</p>
                <p class = "justified" style ="color:gray;"><ul style ="color:gray;">
                                            <li>You’d feel like a fish out of water without a well defined traditional corporate structure.</li>
                                            <li>If you do not have an entrepreneurial bone in you.</li>
                                            <li>If you see yourself selling ice cream, instead of changing the world.</li>
                                            <li>You are not a risk taker. We want the best people that are aggressive, 
                                                not afraid of taking chances, and play offense all the time.</li>
                                        </ul>
                </p>
              </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">You have applied as a Product Manager.</h4>
            </div>

            <div class="modal-body">
                <p class = "justified" style ="color:gray;">At a startup, everyone does a little of everything, so we need strong generalists. More importantly, it’s hard to predict the future, so we need people who can adapt.</p>
                <p class = "justified" style ="color:gray;">A great product manager usually makes the difference between winning and losing. Product management combines elements of other specialties - engineering, design, marketing, sales, business development. Product management is a weird discipline full of oddballs and rejects that never quite fit in anywhere else.</p>
                <p class = "justified" style ="color:gray;">So what do we look for in a PM?</p>
                <p class = "justified" style ="color:gray;">Raw intellectual horsepower. We’ll take a wickedly smart, inexperienced PM over one of average intellect and years of experience any day. Product management is fundamentally about thinking on your feet, staying one step ahead of your competitors, and being able to project yourself into the minds of your colleagues and your customers.</p>
                <p class = "justified" style ="color:gray;">This next hire will "own" a product that will generate a lot of money. We want to make sure we get it right.</p>
                <p class = "justified"><b>Requirements:</b></p>
                <p class = "justified" >
                  <ul style ="color:gray;">
                    <li>Candidate must possess at least a Bachelor’s/College Degree , Computer Science/Information Technology, Engineering (Computer/Telecommunication) or equivalent.</li>
                    <li>Must be willing to learn web application development</li>
                    <li>Understanding of the internet industry and competing products</li>
                    <li>The product and business savvy mindset to pinpoint what metric matters most and the communication skills to convey those thoughts to others</li>
                    <li>Ability to communicate design decisions, code walk-throughs, bugs, and what to have for lunch thoroughly, clearly, and concisely</li>
                    <li>Outstanding team skills with the desire to work with other engineers to ensure product consistency and maintainability</li>
                    <li>A knack for reverse engineering</li>
                    <li>Ability to adapt, take critical feedback, and execute quickly on tasks</li>
                    <li>Previous experience building a website from start to finish</li>
                    <li>Required skill(s): PHP, MySQL, HTML, CSS</li>
                    <li>Preferred skill(s): Flash, AJAX, JAVA, C++, C#</li>
                    <li>A sense of humo(u)r =p</li>
                    <li>Applicants must be willing to work in Taguig City</li>
                    <li>2 Full-Time, Part-Time and Internship position available</li>
                  </ul>
                </p>
                <p class = "justified"><b>So, what’s next?</b></p>
                <p class = "justified" style ="color:gray;">We are currently filtering applicants for this position. The ensuing questions will help us determine who would best fit the position.</p>
              </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="examModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Product Manager Initial Questionnaire</h4>
            </div>

            <div class="modal-body">
                <form id="marketingInternForm" method="post" action="processor.php" enctype="multipart/form-data">
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="First Name" name="firstName">
                      <input type="hidden" name="seta_type" value = "9">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Last Name" name="lastName">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="email" class="form-control" placeholder="Email Address" name="email">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Contact Number" name="contactNumber" onkeypress = "return isNumber(event)">
                    </div>
                    <div class="form-group input-group full-width">
                      <p class="text-muted">Resume(PDF)</p> 
                      <input type="file" class="form-control" name="file" id="file">
                    </div>
                  <hr>
                  <p class = "justified"><b>Try to answer as many as you can.</b></p>
                  <?php
                    $questions = json_decode($set_data[0]['set_content'],true);
                    foreach($questions as $question){
                  ?>
                    <!--<h4 class="section-heading"><?php echo $question['order'];?></h4> -->
                    <p class="text-muted"><?php echo $question['question'];?></p>
                    <textarea name="FPZ004-<?php echo $question['order'];?>" class="form-control" rows="4"></textarea>
                    <br/>
                    <?php       
                        }
                    ?>


                    <div class="form-group">
                      <br/>
                      <br/>
                        <button type="submit" name="submit_answers" class="btn btn-default">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
  
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/formvalidation/js/formValidation.min.js"></script>
  <script src="bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
  <script type = text/javascript>
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
  </script>
  <script>
    $(document).ready(function() {
      $('#marketingInternForm').formValidation({
        framework: 'bootstrap',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          firstName: {
            validators: {
              notEmpty: {
                message: 'First Name is required'
              }
            }
          },
          lastName: {
            validators: {
              notEmpty: {
                message: 'Last Name is required'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Email Address is required'
              },
              emailAddress: {
                message: 'The input is not a valid email address'
              }
            }
          },
          contactNumber: {
            validators: {
              notEmpty: {
                message: 'Contact Number is required'
              }
            }
          },
          file: {
            validators: {
              notEmpty: {
                message: 'Resume is required'
              }
            }
          }
        }
      });
    });
  </script>
  
  </body>
</html>

<?php 
require_once('ignition.php');

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ005'";
$set_data = $database->query($query);


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
  
  <link rel="stylesheet" href="bootstrap/formvalidation/css/formValidation.min.css">
    <link href="bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="internheader">
      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">So you wanna join the team.</h1><br>
        <p class="centered" style="color:#fff;font-size:20px;">Part of our core value is efficiency in everything we do. This includes how we add new members to the team.<br/>
        We built this tool to help us efficiently filter the winners from the posers. Shall we start?</p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns --><p class = "whitespace"></p>
        <h2 class="centered" style="font-size:30px;">You applied as a UI/UX Designer.</h2><br>
        <p class = "centered" style="font-size:20px;"><a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#startModal">Who we are. What we need.</a></p>
            <center><p><a class="btn btn-default" href="#" role="button" data-toggle="modal"  data-target="#examModal">Let's Start &raquo;</a></p></center>
       </div>
   

      <hr>

     <footer>
        <p align="center" style = "font-size:15px;">&copy; An Innovation Fridays Project, by <font color="blue"><a href="http://www.fourptzero.com" target="_blank"> FourPoint.Zero, Inc. 2015</a></font></p>
      </footer>
    </div> <!-- /container -->

    <!-- Modal
    ================================================== -->
    <div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class = "tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab">Introduction</a></li>
                <li><a href="#tab2" data-toggle="tab">Requirements</a></li>
              </ul>
          <div class = "tab-content">
          <div class="tab-pane active" id="tab1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">We are FourPoint.Zero. We need a UI/UX Designer.</h4>
            </div>

            <div class="modal-body">
                <p class = "justified"><b>Let us first introduce ourselves.</b></p>
                <p class = "justified" style ="color:gray;">We're a small, energetic team that is building products that actually adds value to a lot of people. 
                    We're small enough to be focused and effective even while we're growing quickly. 
                    We put a lot of effort into making our home base a fun place for working and getting things done; we only hire when the timing and person are right.</p>
                <p class = "justified" style ="color:gray;">FourPoint.Zero is a small technology company, bootstrapped, profitable, and chaotic good.</p>
                <p class = "justified" style ="color:gray;">We want someone who loves to do great work, who loves to think, who loves new opportunities, and who wants to change the world.</p>
                <p class = "justified"><b>Our Culture.</b></p>
                <p class = "justified" style = "color:gray;">FourPoint.Zero’s culture is focused on entrepreneurship. 
                    No matter how good you are when you come in, it is our goal to make every day a day for you to challenge yourself, 
                    find opportunities to create new business, and make yourself some money. 
                    Coding, programming, marketing, show-and-tell — whatever it takes, we want you to do it to make you a success.<br/><br/>
                    We don’t offer competitive salaries, and fancy health benefits… but we offer stock options, partnerships, real world learning experience, 
                    flexible hours, a fun and revitalizing work place, and opportunities for you to follow your entrepreneurial dreams.</p>
                <p class = "justified" ><ul style ="color:gray;">
                                            <li>Work on products that affects people every day.</li>
                                            <li>Work with a bunch of cool people who love doing what they do.</li>
                                            <li>Need a break? Get out of the office, swim, jog, play video games, chill.</li>
                                            <li>Innovation Fridays! Fridays are not normal work days - you can take the half day off, 
                                                work on your own project/product, or work your sidelines.</li>
                                        </ul>
                </p>
                <p class = "justified" style ="color:gray;">Please DO NOT apply if:</p>
                <p class = "justified" style ="color:gray;"><ul style ="color:gray;">
                                            <li>You’d feel like a fish out of water without a well defined traditional corporate structure.</li>
                                            <li>If you do not have an entrepreneurial bone in you.</li>
                                            <li>If you see yourself selling ice cream, instead of changing the world.</li>
                                            <li>You are not a risk taker. We want the best people that are aggressive, 
                                                not afraid of taking chances, and play offense all the time.</li>
                                        </ul>
                </p>
              </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">You have applied as a UI/UX Designer.</h4>
            </div>

            <div class="modal-body">
                <p class = "justified" style ="color:gray;">We are looking for our first Director of User Experience. This is a great opportunity for someone who is skilled at creating intuitive and engaging mobile and web applications. We are looking for someone ready to take on the challenge of extending our commitment to deliver beautiful designs across multiple web and mobile platforms, vertical applications and partner requirements.</p>
                <p class = "justified" style ="color:gray;">We are seeking a creative and energetic designer who understands fundamental usability and aesthetic principles and best practices to design, effectively develop, and define our web and mobile user interfaces. The position requires a deep understanding of strategy, usability, interaction and visual design.</p>
                <p class = "justified" style ="color:gray;">We are seeking a passionate pixel perfectionist with a strong design background who approaches design with a simple, fun, and intuitive mindset. If you enjoy conceptualizing and designing clean interfaces then you’ll feel right at home. See if you have what it takes to design for us:
                  <ul style ="color:gray;">
                    <li>You love simple, usable interfaces and appreciate the hard work that goes behind creating them.</li>
                    <li>You love to see your work come to life in front of a large audience.</li>
                    <li>You love to understand the behavior behind an interface rather than just using it.</li>
                    <li>You subscribe to the Less Is More design philosophy.</li>
                  </ul>
                </p>
                <p class = "justified" style ="color:gray;">If you find the start-up world interesting and dynamic to work in this may be your gig. The environment is extremely fast paced with a continual demand for problem solving logic and fresh new ideas. The team is small and your ability to make an impact is massive! As one of the initial team members you will have the ability to build the voice of the company and make a lasting impression.</p>
                <p class = "justified" style ="color:gray;">We believe in designing and building cool things for cool people. In order to do that more effectively, we need to add more cool people to our team. If you think you’re the right fit, show us what you got.</p>
                <p class = "justified" style ="color:gray;">
                  As a UI/UX Web/Graphics Designer, you should be familiar with the following:
                    <ul style ="color:gray;">
                      <li>Adobe CS</li>
                      <li>Web design standards</li>
                      <li>Latest design trends</li>
                      <li>HTML/CSS</li>
                      <li>Motion graphics would be awesome, but not needed</li>
                    </ul>
                </p>
                <p class = "justified" style ="color:gray;">
                  You will be measured by your ability to:
                    <ul style ="color:gray;">
                      <li>Design and implement beautiful, elegant and effective web, mobile and traditional media designs.</li>
                      <li>Define strategies aligned with business goals and market environment to create compelling and innovative user experiences.</li>
                      <li>Remain current on user interface design trends and designing techniques.</li>
                    </ul>
                </p>
                <p class = "justified" style ="color:gray;">
                  Ideally, you’ll:
                    <ul style ="color:gray;">
                      <li>Plan, design and measure the user experience for customer acquisition, usage and retention of web, mobile, corporate and print projects.</li>
                      <li>Provide a compelling brand experience with strong sense of aesthetics.</li>
                      <li>Conduct user requirements analysis with project stakeholders to identify key current and future customer needs.</li>
                      <li>Develop conceptual diagrams, wireframes, interaction flows, and visual mock-ups and prototypes based on business requirements.</li>
                      <li>Work closely with the Marketing team to provide any creative support for online and print marketing collaterals.</li>
                    </ul>
                </p>
                <p class = "justified" style ="color:gray;">
                  You’ll be able to:
                    <ul style ="color:gray;">
                      <li>Design experiences that delight users.</li>
                      <li>Think creatively.</li>
                      <li>Be responsible for designing a total customer experience.</li>
                      <li>Think through all the interaction points between customers and the product.</li>
                      <li>Attempt to evoke emotions in users of the product and then monitor whether your attempts hit the mark.</li>
                      <li>Assist users to experience an emotion and understand that content is a manipulation of the user’s behavior.</li>
                      <li>Implement. Write and maintain HTML and CSS code. If needed.</li>
                    </ul>
                </p>
                <p class = "justified" style ="color:gray;">You should be willing to have fun, and enjoy an open office atmosphere. You should be able to work on a couch, a bean bag, on a veranda, beside the pool; instead of a boring office cubicle. Oh, and you should bring your own laptop for now.</p>
                <p class = "justified" style ="color:gray;">We are committed to rewarding our staff with monthly profit-sharing bonuses that is tied in to performance and project margins. We’re looking out for passionate geniuses to form our core team.</p>
                <p class = "justified" style ="color:gray;">Bottom line? We’re growing fast. Join our crew and have some fun making money with us.</p>
                <p class = "justified" style ="color:gray;">On an ordinary day, we would require experience in UI/UX design, understanding of methods for designing easy-to-use interfaces, including user and task analysis, information architecture, interface design, heuristic evaluation, and usability testing; along with strong communication, analytical, and interpersonal skills working within cross-functional teams; and proven ability to balance multiple projects while meeting tight deadlines; especially a strong passion for creating world-class web/mobile/print experiences. But, at this time, good portfolio of work is all we care about.</p>
                <p class = "justified"><b>Requirements</b></p>
                <p class = "justified" >
                  <ul style ="color:gray;">
                    <li>Education: Does not matter if you have talent and skill.</li>
                    <li>Exceptional understanding of fundamental design disciplines (composition, information architecture, color theory, typography and animation) and principals of good user interface design (consistency, simplicity, usability, etc.).</li>
                    <li>Solid Adobe CS skills</li>
                    <li>Understanding of the internet industry and competing products</li>
                    <li>Ability to adapt, take critical feedback, and execute quickly on tasks</li>
                    <li>Geek mentality :)</li>
                    <li>Applicants must be willing to work in Taguig City</li>
                    <li>Fresh graduates/Entry level applicants are encouraged to apply.</li>
                    <li>2 Full-Time and Contract position available.</li>
                  </ul>
                </p>
                <p class = "justified"><b>So, what’s next?</b></p>
                <p class = "justified" style ="color:gray;">We are currently filtering applicants for this position. The ensuing questions will help us determine who would best fit the position.</p>
              </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="examModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">UI/UX Designer Initial Questionnaire</h4>
            </div>

            <div class="modal-body">
                <form id="marketingInternForm" method="post" action="processor.php" enctype="multipart/form-data">
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="First Name" name="firstName">
                      <input type="hidden" name="seta_type" value = "7">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Last Name" name="lastName">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="email" class="form-control" placeholder="Email Address" name="email">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Contact Number" name="contactNumber" onkeypress = "return isNumber(event)">
                    </div>
                    <div class="form-group input-group full-width">
                      <p class="text-muted">Resume(PDF)</p> 
                      <input type="file" class="form-control" name="file" id="file">
                    </div>
                  <hr>
                  <p class = "justified"><b>Try to answer as many as you can.</b></p>
                  <?php
                    $questions = json_decode($set_data[0]['set_content'],true);
                    foreach($questions as $question){
                  ?>
                    <!--<h4 class="section-heading"><?php echo $question['order'];?></h4> -->
                    <p class="text-muted"><?php echo $question['question'];?></p>
                    <textarea name="FPZ005-<?php echo $question['order'];?>" class="form-control" rows="4"></textarea>
                    <br/>
                    <?php       
                        }
                    ?>


                    <div class="form-group">
                      <br/>
                      <br/>
                        <button type="submit" name="submit_answers" class="btn btn-default">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
  
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="bootstrap/formvalidation/js/formValidation.min.js"></script>
  <script src="bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
  <script type = text/javascript>
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
  </script>
  <script>
    $(document).ready(function() {
      $('#marketingInternForm').formValidation({
        framework: 'bootstrap',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          firstName: {
            validators: {
              notEmpty: {
                message: 'First Name is required'
              }
            }
          },
          lastName: {
            validators: {
              notEmpty: {
                message: 'Last Name is required'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Email Address is required'
              },
              emailAddress: {
                message: 'The input is not a valid email address'
              }
            }
          },
          contactNumber: {
            validators: {
              notEmpty: {
                message: 'Contact Number is required'
              }
            }
          },
          file: {
            validators: {
              notEmpty: {
                message: 'Resume is required'
              }
            }
          }
        }
      });
    });
  </script>
  
  </body>
</html>

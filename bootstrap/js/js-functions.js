
    $(document).ready(function() {
//AJAX START

//READ LOG START
$.fn.readlog = function(logid){
  $.ajax({
    url: "process-logmaint.php",
    type: "post",
    dataType: "json",
    data: {
      readlogid : logid
    },
    success: function(response){
      console.log(response);
    }
  });
}
//READ LOG END

//GET USER DETAILS FOR EDITING START
$.fn.getUserdetails = function(userid){
  $.ajax(
  {
    url: "process-usermaint.php",
    type: "post",
    dataType: "json",
    data: {
      userdetail_id : userid
    },
    success: function(response){
      var fullname = response[0]['user_fname'] + " " + response[0]['user_lname'] + "'s";
      var level = response[0]['user_position'];
      $("#userfullname").html(fullname);
      $("#edituserid").val(response[0]['user_id']);
      $("#edituserLname").val(response[0]['user_lname']);
      $("#edituserFname").val(response[0]['user_fname']);
      $("#edituserPos option[value="+response[0]['user_position']+"]").attr("selected", "selected");
      $("#edituserLevel option[value="+response[0]['user_level']+"]").attr("selected", "selected");
      $("#edituserEmail").val(response[0]['user_email']);
      $("#editsetmail option[value="+response[0]['user_setmail']+"]").attr("selected", "selected");
      if(level == 'admin'){
        $("#edituserPos").attr("disabled", true);
      }
      else{
        $("#edituserPos").attr("disabled", false);
      }
    }
  });
}
//GET USER DETAILS FOR EDITING END


//GET VALUES FOR ACCOUNT SETTINGS START
$.fn.getUserset = function(userset_id, userset_uri){
  $.ajax(
  {
    url: "process-usermaint.php",
    type: "post",
    dataType: "json",
    data: {
      userset_id : userset_id,
      userset_uri  : userset_uri
    },
    success: function(response){
      setTimeout(function(){
        $("#acct_userid").val(response['userid']);
        $("#acct_useruri").val(response['theuseruri']);
        $("#txtLname").val(response['userLname']);
        $("#txtFname").val(response['userFname']);
        $("#txtEmail").val(response['userEmail']);
        $("#txtPos").val(response['userPos']);
        $("#txtLevel").val(response['userLevel']);
        $("#setmail option[value="+response['userSetmail']+"]").attr("selected", "selected");
      }, 100);
    }
  });
}
//GET VALUES FOR ACCOUNT SETTINGS END

//GET VALUES FOR CHANGE PASS START
  $.fn.getUser = function(user_id, uri, user_pass){
    $.ajax(
    {
      url: "process-usermaint.php",
      type: "post",
      dataType: "json",
      data: {
        pass_user_id : user_id,
        pass_user_pass : user_pass,
        pass_user_uri : uri
      },
      success: function(response){
        setTimeout(function(){
          $("#chg_userid").val(response["user_id"]);
          $("#chg_useruri").val(response["the_uri"]);
          var oldpass = response["useruniquecode"];
          console.log(oldpass);
          $("#txtOldPass").keyup(function(){
            if($(this).val() != oldpass){
              this.setCustomValidity("Password doesn't match old password");
            }
            else{
              this.setCustomValidity("");
            }

          });
          var password = document.getElementById("txtNewPass");
          var confirm_password = document.getElementById("txtConfirmPass");

          function validatePassword(){
            if(password.value != confirm_password.value) {
              confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
              confirm_password.setCustomValidity('');
            }
          }

          password.onchange = validatePassword;
          confirm_password.onkeyup = validatePassword;
        }, 100);
      }
    });
  }
//GET VALUES FOR CHANGE PASS END

//UPDATE POSITION START
      $(".editPos").click(function(){
      var valpos = $(this).data("id");
        $.ajax(
        {
          url: "process-updatepos.php",
          type: "post",
          dataType: "json",
          data: { editPosid: valpos },
          success: function(response){
            setTimeout(function(){
            $("#posinfo").html(response[0]["set_type_desc"]);
            $("#editposid").val(response[0]["set_type_id"]);
            $("#editposName").val(response[0]["set_type_desc"]);
            $("#editposSlug").val(response[0]["set_type_slug"]);
            $("#editposType option[value="+response[0]["set_postype"]+"]").attr("selected", "selected");
            $("#editposRounds").val(response[0]["set_type_rounds"]);

            }, 100);
          }
        });
    });
//UPDATE POSITION END

//GET ROUND FOR QUESTIONNAIRE START
      $.fn.getQuestions = function(select_posid){
        $.ajax(
        {
          url: "process-updateQuestions.php",
          type: "post",
          dataType: "json",
          data: { 
            select_posid : select_posid
          },
          success: function(response){
            setTimeout(function(){
              $('#round_header').html("");
              $(".questioncontainer").html("");
              $("#pos-title").html(response[0]['set_type_desc']);
              $("#qpos_id").val(response[0]['set_type_id']);
              $(".roundctrl").html("");
              $(".roundctrl").append("<option value='' disabled selected>---Select Questionnaire Round---</option>");
              for(var roundCtr = 1; roundCtr <= response[0]['set_type_rounds']; roundCtr++){
                var html = "<option value='"+roundCtr+"'>Round "+roundCtr+"</option>";
                $(".roundctrl").append(html);
              };
            }, 100);
          }
        });
      };
//GET ROUND FOR QUESTIONNAIRE END

//DISPLAY AND EDIT QUESTIONNAIRE START
      $(".roundctrl").change(function(){
        var setid = $("#qpos_id").val();
        var setround = $(".roundctrl option:selected").val();
        var setdesc = $("#pos-title").html();
        $('#round_header').html(setdesc+" Round "+setround);
        $.ajax(
        {
          url: "process-updateQuestions.php",
          type: "post",
          dataType: "json",
          data: {
            set_id : setid,
            set_round : setround
          },
          success: function(response){
            $(".questioncontainer").html("");
            if(response !== null){
              var i = 0;
              for (i in response){
                response.sort(function(a,b){
                  return a.order - b.order;
                });
                var print_question = "<tr class = 'center'>";
                    print_question += "<td><input name = 'order[]' type = 'text' class = 'qnum center' value = "+response[i]['order']+" required /></td>";
                    print_question += "<td><textarea name = 'question[]' id = 'editor"+i+"'class = 'qdesc editable center' placeholder = 'Insert Question Here' required>"+response[i]['question']+"</textarea></td>";
                    print_question += "<td><input name = 'type[]' type = 'text' class = 'qtype center' value = "+response[i]['type']+" required /></td>";
                    print_question += "</tr>";
                $(".questioncontainer").append(print_question);
              }
              $('.editable').click(function(){
              CKEDITOR.replace(this);
              CKEDITOR.instances[$(this).attr("id")].on('blur', function(){
                CKEDITOR.instances[$(this).attr("name")].destroy();
              });
              });
              var counter = response.length;
              $("#the_addbutton").html("");
              $("#the_addbutton").append("<a href = '#' id = 'add_button' class = 'btn btn-primary'>Add Question</a>");
              $("#add_button").click(function(){
                counter++;
                console.log("add"+counter);
                var add_question = "<tr class = 'center' id = 'addrow"+counter+"'>";
                    add_question += "<td><input name = 'order[]' type = 'text' class = 'qnum center' value = "+counter+" required /></td>";
                    add_question += "<td><textarea name = 'question[]' id = 'editor"+counter+"'class = 'qdesc editable2 center' placeholder = 'Insert Question Here' required></textarea></td>";
                    add_question += "<td><input name = 'type[]' type = 'text' class = 'qtype center' value = '' required><img src = '/bootstrap/images/close-button.png' id = '"+counter+"' class = 'btnremove'/></td>";
                    add_question += "</tr>";
                $(".questioncontainer").append(add_question);

                  $('.editable2').click(function(){
                    CKEDITOR.replace(this);
                    CKEDITOR.instances[$(this).attr("id")].on('blur', function(){
                      CKEDITOR.instances[$(this).attr("name")].destroy();
                    });
                    });
                  $(".btnremove").click(function(){
                    var choice = confirm("Press a button!");
                    if (choice == true){
                      var rowcount = $(this).attr("id");
                      $("#addrow"+rowcount).remove();
                      counter = rowcount - 1;

                    }
                    else {
                      console.log(choice);
                    }
                  });
              });
                    
            }
            else{
              var counter = 0;
              $("#the_addbutton").html("");
              $("#the_addbutton").append("<a href = '#' id = 'add_button' class = 'btn btn-primary'>Add Question</a>");
              $("#add_button").click(function(){
                counter++;
                var add_question = "<tr class = 'center'>";
                    add_question += "<td><input name = 'order[]' type = 'text' class = 'qnum center' value = "+counter+" required /></td>";
                    add_question += "<td><textarea name = 'question[]' id = 'editor"+counter+"'class = 'qdesc editable center' placeholder = 'Insert Question Here' required></textarea></td>";
                    add_question += "<td><input name = 'type[]' type = 'text' class = 'qtype center' value = '' required /></td>";
                    add_question += "</tr>";
                $(".questioncontainer").append(add_question);

                  $('.editable').click(function(){
                    CKEDITOR.replace(this);
                    CKEDITOR.instances[$(this).attr("id")].on('blur', function(){
                      CKEDITOR.instances[$(this).attr("name")].destroy();
                    });
                    });
              });
              
            }
          }
        });
      });
//DISPLAY AND EDIT QUESTIONNAIRE END

//REJECT APPLICANT MODAL START
    $.fn.rejectapp = function(rejectapplicant, rejectreference){
      
        $.ajax(
        {
          url: "process-getApplicantformodal.php",
          type: "post",
          dataType: "json",
          data: { 
            rejectappid: rejectapplicant,
            rejectref: rejectreference 

          },
          success: function(response){
            setTimeout(function(){
              $("#rejectappid").val(response[0]["applicant_id"]);
              $("#rejectappref").val(response["appref"]);
              $("#rejectAppName").val(response[0]["applicant_first_name"] + " " + response[0]["applicant_last_name"]);
              $("#rejectAppPos").val(response[1]["set_type_desc"]);
              $("#rejectAppStat").val(response[0]["applicant_status"]);


            }, 100);
          }
        });
    };
//REJECT APPLICANT MODAL END

//ACCEPT APPLICANT MODAL START
    $.fn.acceptapp = function(acceptapplicant, acceptreference){
      
        $.ajax(
        {
          url: "process-getApplicantformodal.php",
          type: "post",
          dataType: "json",
          data: { 
            acceptappid: acceptapplicant,
            acceptref: acceptreference 

          },
          success: function(response){
            setTimeout(function(){
              var appround = response[0]["applicant_phase"];
              var posrounds = response[1]["set_type_rounds"];
              var appstat = response[0]["applicant_status"];
              $("#lblcomment").html("");
              for(var i = 0; i <= posrounds; i++){
                if(appstat == "New Applicant"){
                  if(!$("#rate_star").is(":visible")){
                    $("#rate_star").show();
                    $("#rndscore").show();
                    $("#the_rating").attr({
                      title: "Stars are required. Oh so shiny stars!",
                      required: ""
                    });
                  }
                  $("#lblcomment").html("Insert Comment/Review For New Applicant (Optional):");
                }
                else if(appstat == "For Stage "+i+" Review"){
                  if(!$("#rate_star").is(":visible")){
                    $("#rate_star").show();
                    $("#rndscore").show();
                    $("#the_rating").attr({
                      title: "Stars are required. Oh so shiny stars!",
                      required: ""
                    });
                  }
                  if(appround == posrounds){
                    $("#lblcomment").html("Insert Comment/Review For Interview (Optional):")
                  }
                  else{
                    $("#lblcomment").html("Insert Comment/Review For Round "+appround+" (Optional):");
                  }
                }
                else if(appstat == "For Interview"){
                  if($("#rate_star").is(":visible")){
                    $("#rate_star").hide();
                    $("#rndscore").hide();
                    $("#the_rating").removeAttr("title required");
                  }
                  $("#lblcomment").html("Insert Remarks Below (Optional):");
                }
                else if(appstat == "Unsuccessful"){
                  if(!$("#rate_star").is(":visible")){
                    $("#rate_star").show();
                    $("#rndscore").show();
                    $("#the_rating").attr({
                      title: "Stars are required. Oh so shiny stars!",
                      required: ""
                    });
                  }
                  $("#lblcomment").html("Insert Comment Below (Optional):");
                }
              }

              $("#acceptappid").val(response[0]["applicant_id"]);
              $("#acceptappref").val(response["appref"]);
              $("#acceptAppName").val(response[0]["applicant_first_name"] + " " + response[0]["applicant_last_name"]);
              $("#acceptAppPos").val(response[1]["set_type_desc"]);
              $("#acceptAppStat").val(response[0]["applicant_status"]);


            }, 100);
          }
        });
    };
//ACCEPT APPLICANT MODAL END

//GET APPLICANT COMMENTS START
    $.fn.appcomm = function(appcommid){
      
        $.ajax(
        {
          url: "process-getApplicantformodal.php",
          type: "post",
          dataType: "json",
          data: { 
            appcomment: appcommid,

          },
          success: function(response){
            setTimeout(function(){
              var appstat = response[0]["applicant_status"];
              $("title-comment").html("");
              if(appstat == "Unsuccessful"){
                $("#title-comment").html("Reason For Rejection");
              }
              else{
                $("#title-comment").html("Applicant Review");
              }
              $("#commentAppName").val(response[0]["applicant_first_name"] + " " + response[0]["applicant_last_name"]);
              $("#commentAppPos").val(response[1]["set_type_desc"]);
              $("#commentAppStat").val(response[0]["applicant_status"]);
              if (response[0]["applicant_comment"] == "") {
                $("#appcomment").addClass('redtext');
                $("#appcomment").html("No comment");
              }
              else{
              $("#appcomment").removeClass('redtext');
              $("#appcomment").html(response[0]["applicant_comment"]);
              }
              $(function() {
                  $('span.stars').stars();
              });
            }, 100);
          }
        });
    };
//GET APPLICANT COMMENTS END

//AJAX END

//datatables START
    $('.example').dataTable({

    });
    $('.tblLogs').dataTable({
      "order": [[ 2, "desc" ]]
    });
//datatables END

//Star Rating START
$('.radiorate').change(
  function(){
    $('.choice').text( this.value + ' star(s)' );
  } 
);
$.fn.stars = function() {
    return $(this).each(function() {
        $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
    });
}
$(function() {
    $('span.stars').stars();
});
//Star Rating END


//Select Option START
    function changeMe(sel)
    {
      sel.style.color = "#000";              
    }
//Select Option END

//Text-field number only input START
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
//Text-field number only input END


//VALIDATE FORM START
  $('#addUserForm').formValidation({
        framework: 'bootstrap',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          userLname: {
            validators: {
              notEmpty: {
                message: 'Last Name is required'
              }
            }
          },
          userFname: {
            validators: {
              notEmpty: {
                message: 'First Name is required'
              }
            }
          },
          userPos: {
            validators: {
              notEmpty: {
                message: 'Position is required'
              }
            }
          },
          userLevel: {
            validators: {
              notEmpty: {
                message: 'Level is required'
              }
            }
          },
          userEmail: {
            validators: {
              notEmpty: {
                message: 'E-mail Address is required'
              },
              emailAddress: {
                message: 'Bruuuhhh that\'s not a valid e-mail address.'
              }
            }
          },
          userSetmail: {
            validators: {
              notEmpty: {
                message: 'Set Mail Notification is required'
              }
            }
          }
        }
      });


      $('#addPosForm').formValidation({
        framework: 'bootstrap',
        icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          posName: {
            validators: {
              notEmpty: {
                message: 'Position Name is required'
              }
            }
          },
          posSlug: {
            validators: {
              notEmpty: {
                message: 'Position Slug is required'
              }
            }
          },
          posActive: {
            validators: {
              notEmpty: {
                message: 'Hiring Status is required'
              }
            }
          },
          posType: {
            validators: {
              notEmpty: {
                message: 'Position Type is required'
              }
            }
          },
          posRounds: {
            validators: {
              notEmpty: {
                message: 'Position Questionnaire Rounds is required'
              }
            }
          }
        }
      });
    });
//VALIDATE FORM END
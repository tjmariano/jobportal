<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";

$loc = get_home();
$curr_date = date('Y-m-d H:i:s');

if (isset($_POST['seta_type'])) { 
	$pos_type = $_POST['seta_type'];

if($pos_type === "1"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ001-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			

			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ001',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 1
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Marketing Internship Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");


		$to2= "zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, sebastian.mapy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 = "Marketing Intern Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as marketing intern.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

elseif($pos_type === "2"){


		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ001-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			
			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ001',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 2
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);

				
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Marketing Executive Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions. Please expect an email from us in a day or two regarding your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");

		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 ="Marketing Executive Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as marketing executive.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

elseif($pos_type === "3"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ001-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			
			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ001',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 3
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);

				
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Java Developer Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions. Please expect an email from us in a day or two regarding your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");

		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 ="Java Developer Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as Java Developer.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

elseif($pos_type === "4"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ004'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ004-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			

			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ004',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 4
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Software Engineer Internship Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");


		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 = "Software Engineer Intern Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as software engineer intern.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
	}

elseif($pos_type === "5"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ004'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ004-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			
			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ004',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 5
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);

				
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Software Engineer Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions. Please expect an email from us in a day or two regarding your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");

		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 ="Software Engineer Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as Software Engineer.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

elseif($pos_type === "6"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ005'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ005-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			

			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ005',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 6
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - UI/UX Designer Internship Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");


		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 = "UI/UX Designer Intern Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as UI/UX designer intern.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
	}

elseif($pos_type === "7"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ005'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ005-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			
			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ005',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 7
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);

				
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - UI/UX Designer Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions. Please expect an email from us in a day or two regarding your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");

		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 ="UI/UX Designer Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as UI/UX Designer.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

elseif($pos_type === "8"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ004'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ004-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			

			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ004',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 8
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Management Trainee Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions.</p>
		<p>Expect an email from us in a day or two regarding the status of your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");


		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 = "Management Trainee Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as management trainee.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
	}

elseif($pos_type === "9"){

		$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ004'";
		$set_data = $database->query($query);

		if(isset($_POST['submit_answers'])){
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = 'FPZ004-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
				//$answers["$question['order']"] = $_POST[$question_id];
			}
			
			
			$answers_obj = array(
				'seta_token'		=> md5($curr_date),
				'seta_first_name'	=> $_POST['firstName'],
				'seta_last_name'	=> $_POST['lastName'],
				'seta_email'		=> $_POST['email'],
				'seta_contact'		=> $_POST['contactNumber'],
				'seta_date_taken'	=> $curr_date,
				'seta_company'		=> 1,
				'seta_set'			=> 'FPZ004',
				'seta_answer'		=> json_encode($answers),
				'seta_type'			=> 9
			);	
			
			if($database->insertRow('navy_set_answer', $answers_obj)){
				$query = "SELECT seta_id FROM navy_set_answer WHERE seta_email = '".$_POST['email']."' AND seta_date_taken = '$curr_date'";
				$data = $database->query($query);

				
				
				$allowedExts = array("pdf","PDF");
				$temp = explode(".", $_FILES["file"]["name"]);
				$extension = end($temp);
				if (($_FILES["file"]["type"] == "application/pdf")&&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
					if ($_FILES["file"]["error"] > 0){
						echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
					}else{
						/*echo "Uploaded: " . "$root/uploads/" .$data[0]['seta_id'].".pdf" . "<br>";*/
						move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$data[0]['seta_id'].".pdf");
					}
				}
				
				$loc = get_thankYouPage();
			}else{
				$loc = get_errorPage();
			}
		}

		$to = $_POST['email'];
		$subject ="FourPoint.Zero, Inc. - Product Manager Application";

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Application</title>
		</head>
		<body>
		<p>Hi ".$_POST['firstName']."!</p>
		<p>Thank you for your time to answer our questions. Please expect an email from us in a day or two regarding your application.<p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

		mail($to,$subject,$message,$headers);

		header("Location: $loc");

		$to2= "sebastian.mapy@gmail.com, zyratotzx06@gmail.com, zmjprogoso@yahoo.com, prince.ryan.sy@gmail.com, marlon@fourptzero.com, jobs@fourptzero.com";
		$subject2 ="Product Manager Application (Round 1) - ".$_POST['firstName']." ".$_POST['lastName'];

		$message = "
		<html>
		<head>
		<title>FourPoint.Zero Applicant</title>
		</head>
		<body>
		<p>Hi Admin!</p><br>
		<p>".$_POST['firstName']." ".$_POST['lastName']." is applying as product manager.</p>
		<p>To approve/disapprove the application, Please click the link below.<p>
		<p><a href='".get_home()."login.php' target='_blank'>".get_home()."login.php</a></p><br><br>

		<p>Regards<p>
		<p>FPZ Hiring App<p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <PleaseDontReply@fourptzero.com>' . "\r\n";

		mail($to2,$subject2,$message,$headers);

		header("Location: $loc");

		exit;
}

}

else{
	echo "Wrong Parameter";
}
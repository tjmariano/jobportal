<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";
session_start();

$curr_date = date('Y-m-d H:i:s');
$loc = get_login();
if(isset($_POST['login'])){
  $user = $_POST['username'];
  $pass = hashpass($_POST['password']);

  $query = "SELECT * FROM navy_users WHERE user_username = '$user' AND user_password = '$pass' AND user_active = 1 AND user_company = 1";
  $user_data = $database->query($query);

  $user_level = $user_data[0]['user_level'];
  $user_fullname = $user_data[0]['user_fname']." ".$user_data[0]['user_lname'];

  $user_count = count($user_data);

  if($user_count == 1){
    if($user_level == 'admin'){
      $_SESSION['username'] = $user_data[0]['user_username'];
      $_SESSION['password'] = $_POST['password'];
      $log_obj = array(
          'log_user' => $user_data[0]['user_id'],
          'log_action' => " has logged in.",
          'log_target' => $user_data[0]['user_id'],
          'log_date' => $curr_date,
          'log_type' => "user",
          'log_company' => 1,
          'log_read' => 1
        );
      $database->insertRow('navy_logs', $log_obj);
      $loc = get_admin();

    }
    elseif($user_level == 'employee'){
      $_SESSION['username'] = $user_data[0]['user_username'];
      $_SESSION['password'] = $_POST['password'];
      $log_obj = array(
          'log_user' => $user_data[0]['user_id'],
          'log_action' => " has logged in.",
          'log_target' => $user_data[0]['user_id'],
          'log_date' => $curr_date,
          'log_type' => "user",
          'log_company' => 1,
          'log_read' => 1
        );
      $database->insertRow('navy_logs', $log_obj);
      $loc = get_admin();
    }
    else{
      $loc = get_login();
    }

  }
  else{
    $loc = get_login();

  }
      header("Location: $loc" );
      exit;
  
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	
	<link href="/bootstrap/css/login.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">

      <form class="form-signin" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Username</label>
        <input name="username" type="text" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Log in</button>
      </form>

    </div> <!-- /container -->

  </body>
</html>

<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";
session_start();

$curr_date = date('Y-m-d H:i:s');

  $user = $_SESSION['username'];

  $query = "SELECT * FROM navy_users WHERE user_username = '$user' AND user_active = 1 AND user_company = 1";
  $user_data = $database->query($query);

  $user_fullname = $user_data[0]['user_fname']." ".$user_data[0]['user_lname'];

$log_obj = array(
          'log_user' => $user_data[0]['user_id'],
          'log_action' => " has logged out.",
          'log_target' => $user_data[0]['user_id'],
          'log_date' => $curr_date,
          'log_type' => "user",
          'log_company' => 1,
          'log_read' => 1
        );
      $database->insertRow('navy_logs', $log_obj);
unset($_SESSION['username']);
unset($_SESSION['password']);

header("Location:".get_login());
exit;
?>
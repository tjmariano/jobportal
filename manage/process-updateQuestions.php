<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");

if (isset($_POST['select_posid'])){
	$pid = $_POST['select_posid'];
	$query = "SELECT * FROM navy_set_type WHERE set_type_id = ".$pid." LIMIT 1";
	$pos_data = $database->query($query);


	echo json_encode($pos_data);
}
elseif(isset($_POST['set_id'])){
	$pid = $_POST['set_id'];
	$pround = $_POST['set_round'];

/*	$query = "SELECT * FROM navy_set_type WHERE set_type_id = ".$pid." LIMIT 1";
	$pos_data = $database->query($query);*/

	$query = "SELECT * FROM navy_set where set_position = ".$pid." AND set_order = ".$pround." LIMIT 1";
	$question_data = $database->query($query);

	$questions = json_decode($question_data[0]['set_content'],true);


	echo json_encode($questions);
}
elseif(isset($_POST['submit_editQuestions'])){
	function cmp($a, $b)
		{
		    if ($a->order == $b->order) {
		        return 0;
		    }
		    return ($a->order < $b->order) ? -1 : 1;
		}
	$loc = get_home()."/manage/pos-maint.php";
	$pid = $_POST['qpos_id'];
	$pround = $_POST['qpos_round'];

	$question_orders = $_POST['order'];
	$questions = $_POST['question'];
	$question_types = $_POST['type'];
	$question_count = count($question_orders);

	$query = "SELECT * FROM navy_set WHERE set_position = ".$pid." AND set_order = ".$pround;
	$update_questionnaire = $database->query($query);

	$question_id = $update_questionnaire[0]['set_id'];

	$questionnaire = array();
	for($i = 0; $i < $question_count; $i++){
		$temp = array(
			'order' => $question_orders[$i],
			'question' => $questions[$i],
			'type' => $question_types[$i]
			);
			array_push($questionnaire, $temp);
	}

	$question_obj = array(
			'set_content' => json_encode($questionnaire)
		);
	$question_filter[] = array('set_id=%d', $question_id);
	$database->updateRows('navy_set', $question_obj, $question_filter);

	header("Location: $loc");
	exit;
}

?>
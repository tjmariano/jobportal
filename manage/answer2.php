<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";
session_start();

check_login($_SESSION['username']);

$id = $_GET['id'];

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ002'";
$set_data = $database->query($query);

$query = "SELECT * FROM navy_set_answer WHERE seta_id = ".$id;
$data = $database->query($query);

$query = "SELECT * FROM navy_set2_answer WHERE set2_seta_id = ".$id;
$data2 = $database->query($query);
$applicant_answers = json_decode($data2[0]['set2_answer'],true);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	<link href="/bootstrap/css/jumbotron.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2 class="centered">Personal Information</h2>
          <p>Name: <?php echo $data[0]['seta_first_name']." ".$data[0]['seta_last_name'];?></p>
		  <p>Email: <?php echo $data[0]['seta_email'];?></p>
		  <p>Contact Number: <?php echo $data[0]['seta_contact'];?></p>
		  <p>Date Taken: <?php echo $data[0]['seta_date_taken'];?></p>
		  <p>Video: <a href="<?php echo $data2[0]['set2_link'];?>" target="_blank"><?php echo $data2[0]['set2_link'];?></a></p>
        </div>
        <div class="col-md-8">
          <h2 class="centered">Exam Answers</h2>
		  <?php
			$questions = json_decode($set_data[0]['set_content'],true);
			$question_order = 0;
			foreach($questions as $question){
			?>
			
					<h4 class="section-heading">Question #<?php echo $question['order'];?></h4>
					<p class="text-muted"><?php echo $question['question'];?></p>
					<textarea class="form-control" rows="4" disabled><?php echo $applicant_answers[$question_order][1];?></textarea>
					<br/>
			<?php		
				$question_order++;
			}
			?>
       </div>
      </div>

      <hr>

      <footer>
        <p>&copy; FourPoint.Zero, Inc. 2015</p>
      </footer>
    </div> <!-- /container -->
	
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>	
  </body>
</html>



<?php
require_once("header.php");

$reference = "index.php";

$query = "SELECT * FROM navy_applicant WHERE applicant_status = 'Successful'";
$successful_applicants = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_status = 'New Applicant'";
$new_applicants = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_status LIKE 'For Stage _'";
$pending_answers = $database->query($query);

$answers_count = count($pending_answers);

$query = "SELECT * FROM navy_applicant WHERE applicant_status LIKE 'For Stage _ Review'";
$pending_reviews = $database->query($query);

$reviews_count = count($pending_reviews);

?>
	<div class="col-md-10 main">
        <h1 class="page-header">All Applications</h1>
			<h2 class="sub-header"><?php echo $count_applicants[0]['apps']?> Pending Application(s) / <?php echo $answers_count;?> Pending Answer(s)</h2>
            <div class="tabbable">
			  <ul class="nav nav-tabs">
				<li class="active"><a href="#pane1" data-toggle="tab">New Applicants <?php echo get_countapp($count_newapplicants[0]['apps']);?></a></li>
				<li><a href="#pane2" data-toggle="tab">Pending Answers <?php echo get_countstageapp($answers_count);?></a></li>
				<li><a href="#pane3" data-toggle="tab">Pending Reviews <?php echo get_countapp($reviews_count);?></a></li>
				<li><a href="#pane4" data-toggle="tab">For Interview <?php echo get_countapp($interview_count);?></a></li>
				<li><a href="#pane5" data-toggle="tab">Unsuccessful <?php echo get_countrejectapp($unsuccessful_count);?></a></li>
				<li><a href="#pane6" data-toggle="tab">Successful</a></li>
			  </ul>
			  <br/>
			  <div class="tab-content">
				<div id="pane1" class="tab-pane active">
					<?php print_newapplicantTable($new_applicants,$reference);?>
				</div>
				<div id="pane2" class="tab-pane">
					<?php print_stageTable($pending_answers,$reference);?>
				</div>
				<div id="pane3" class="tab-pane">
				  <?php print_applicantTable($pending_reviews,$reference);?>
				</div>
				<div id="pane4" class="tab-pane">
					<?php print_applicantTable($interview_applicants,$reference);?>
				</div>
				<div id="pane5" class="tab-pane">
				  <?php print_rejectTable($unsuccessful_applicants,$reference);?>
				</div>
				<div id="pane6" class="tab-pane">
				  <?php print_hiredapplicantTable($successful_applicants,$reference);?>
				</div>
			  </div><!-- /.tab-content -->
			</div><!-- /.tabbable -->
        </div>

<?php require_once("footer.php");?>
<?php require_once("pos-modal.php");?>
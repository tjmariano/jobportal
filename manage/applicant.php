<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");
session_start();


$the_user = $_SESSION['username'];
$the_pass = $_SESSION['password'];
check_login($_SESSION['username']);

$current_uri = $_SERVER['REQUEST_URI'];

$app = $_GET['app'];
$query = "SELECT * FROM navy_applicant WHERE applicant_id = $app";
$app_data = $database->query($query);

$appstat = $app_data[0]['applicant_status'];
$phase = $app_data[0]['applicant_phase'];

$query = "SELECT * FROM navy_set_type WHERE set_type_id = ". $app_data[0]['applicant_position'];
$pos_data = $database->query($query);

$rounds = $pos_data[0]['set_type_rounds'];
$pid = $pos_data[0]['set_type_id'];

$reference = "applicant.php?app=$app";

$query = "SELECT * FROM navy_logs WHERE log_type = 'applicant' AND log_read = 0 ORDER BY log_date DESC";
$log_data = $database->query($query);




?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->

  <link href="/bootstrap/css/dashboard.css" rel="stylesheet">
  <link href="/bootstrap/css/cssmenu.css" rel="stylesheet">
  <link href="/bootstrap/css/star-rating.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_admin();?>">FourPointZero</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
                    <?php print_logsdropdown($log_data);?>
                    <?php print_accountdropdown($the_user, $current_uri, $the_pass);?>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar consistent">
            <li style = "text-align: center;">Applicant Review <span class="sr-only">(current)</span></a></li>
            <li class = "details">Name:<br/> <?php echo $app_data[0]['applicant_first_name']." ".$app_data[0]['applicant_last_name'];?></li>
            <li class = "details">Email:<br/> <?php echo $app_data[0]['applicant_email'];?></li>
            <li class = "details">Contact Number:<br/> <?php echo $app_data[0]['applicant_contact'];?></li>
            <li class = "details">Position Applied:<br/> <?php echo $pos_data[0]['set_type_desc'];?></li>
            <li class = "details">Rating:<br/> <span class="stars"><?php echo $app_data[0]['applicant_rating']?></span><p><?php echo $app_data[0]['applicant_rating']?> star(s)</p></li>
            <li class = "details"><a href="<?php echo get_home();?>uploads/<?php echo $app_data[0]['applicant_token'];?>.<?php echo $app_data[0]['applicant_resume']?>" target="_blank">Open Resume</a></li>
            <li class = "details"><a href = "#" data-toggle='modal' data-target='#appcommentModal' onclick = '$.fn.appcomm("<?php echo $app;?>")'>Show Comment</a></li>
          </ul>
        </div>
        
        <div class="col-md-10 main">
          <h1 class="page-header"><?php echo $app_data[0]['applicant_first_name']. " ".$app_data[0]['applicant_last_name'];?></h1>
          
          <div class="sub-header">
          <h2 class = "straight">Status: <?php echo $app_data[0]['applicant_status']?></h2>
          <?php echo get_applicantButtons($app, $reference, $app_data[0]['applicant_status'], $app_data[0]['applicant_phase']);?>
          
          </div>
          

        <div class="tabbable">
        <ul class="nav nav-tabs nav-black">
          <?php
          for($roundCtr = 1; $roundCtr <= $rounds; $roundCtr++){
          $class = "";
          $tabName = "Stage $roundCtr";
          if($roundCtr == 1){
            $class = "class='active'";
            $tabName = "Stage 1";

            print_tabapplicants($class, $tabName, $roundCtr, $phase, $appstat);?>

            

            <?php
          }
          else{
          
          print_tabapplicants($class, $tabName, $roundCtr, $phase, $appstat);?>
        
        <?php
          } 
        }
        ?>
		  
      </ul>
      <br/>
      <div class="tab-content">
      <?php 
      for($roundCtr = 1; $roundCtr <= $rounds; $roundCtr++){
        $status = "";
          $class = "";
          $order_type = 0;
          if($roundCtr == 1){$status = "Stage 1"; $class="active";$order_type = 1;}
          $query = "SELECT * FROM navy_applicant WHERE applicant_id = $app AND applicant_position = $pid  AND applicant_status = '$status'";
          $applicants = $database->query($query);
          


          

          if($roundCtr == 1) { 

              $query = "SELECT * FROM navy_set WHERE set_position = $pid AND set_order = $order_type";
              $temp = $database->query($query);
              $query = "SELECT * FROM navy_answer WHERE ans_applicant = $app AND ans_set = '".$temp[0]['set_code']."'";
              $answers = $database->query($query);

              foreach ($answers as $answer){
                ?>

          <div id="pane<?php echo $roundCtr;?>" class="tab-pane <?php echo $class;?>">
            <?php print_questionnaireWithAnswers($temp[0]['set_content'],$answer['ans_answer']);?>
          </div>

          <?php 
              }

          }

          else {
          $status = "For Stage $roundCtr";
          $order_type = $roundCtr;
          $query = "SELECT * FROM navy_applicant WHERE applicant_id = $app AND applicant_position = $pid  AND applicant_status = '$status'";
          $applicants = $database->query($query);
          $query = "SELECT * FROM navy_set WHERE set_position = $pid AND set_order = $order_type";
          $temp = $database->query($query);
          $query = "SELECT * FROM navy_answer WHERE ans_applicant = $app AND ans_set = '".$temp[0]['set_code']."'";
          $answers = $database->query($query);

          
          
        ?>
          <div id="pane<?php echo $roundCtr;?>" class="tab-pane <?php echo $class;?>">
            <?php foreach ($answers as $answer){
              print_questionnaireWithAnswers($temp[0]['set_content'],$answer['ans_answer']);
            }?>
          </div>

       <?php 
            
          
        }
      } 
        /*foreach($answers as $answer){ 
      
        $query = "SELECT set_content FROM navy_set WHERE set_code = '".$answer['ans_set']."'";
        $temp = $database->query($query);*/ ?>


        
      <?php 
            
      
      
      ?>
        </div>
      
		  </div>
		  
        </div>
       </div>
      </div>



      <?php require_once("footer.php");?>
      <?php require_once("pos-modal.php");?>



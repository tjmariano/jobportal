<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");
session_start();


$the_user = $_SESSION['username'];
check_login($_SESSION['username']);

$user_data = get_currentuser($_SESSION['username']);

$user_id = $user_data['user_id'];
$user_fullname = $user_data['user_lname'].", ".$user_data['user_fname'];
$logtype = 'applicant';

$app_data = get_applicantData($_POST['id']);
$appid = $app_data['applicant_id'];
$position_data = get_positionData($app_data['applicant_position']);



$round = $app_data['applicant_phase'];
$position = $position_data['set_type_id'];
$pos_desc = $position_data['set_type_desc'];

$set_question = get_questionset($round, $position);
$setcode = $set_question['set_code'];

$ansrate = get_answerforrating($appid, $setcode);
$answer_id = $ansrate['ans_id'];

$curr_date = date('Y-m-d H:i:s');

if(isset($_POST['rejected'])){
	$comment = "";
	if(trim($_POST['rejectcomment']) == NULL){
		$comment = "<p class = 'redtext'>No comment</p>";
	}
	else{
		$comment = "<p>".$_POST['rejectcomment']."</p>";
	}
	$appcomment = $app_data['applicant_comment'] . "<p><strong><u>Round ".$round." Reason For Rejection (".$curr_date."):</u></strong></p>" .$comment;
	$applicant_obj = array(
						'applicant_status'	=> "Unsuccessful",
						'applicant_read'	=> 1,
						'applicant_comment' => $appcomment
					);
	
	$applicant_filter[] = array('applicant_id=%d', $_POST['id']);
	if($database->updateRows('navy_applicant', $applicant_obj, $applicant_filter)){
		$mail = array($app_data['applicant_email'],$position_data['set_type_desc'],$app_data['applicant_first_name']);
		send_rejectionMail($mail);

		$app_log = get_applicantData($_POST['id']);
		$appid = $app_log['applicant_id'];
		$appname = $app_log['applicant_first_name']." ".$app_log['applicant_last_name'];
		$appstatus = $app_log['applicant_status'];
		$app_phase = $app_log['applicant_phase'];

		$log_params = array($app_log['applicant_status'], $app_log['applicant_id'], $appname, $app_log['applicant_phase'], $user_fullname, $pos_desc, $curr_date);

		$log_obj = array(
				'log_user' => $user_id,
				'log_action' => get_applicantlog($log_params),
				'log_target' => $app_log['applicant_id'],
				'log_date' => $curr_date,
				'log_type' => 'applicant',
				'log_company' => 1
			);
		$database->insertRow('navy_logs', $log_obj);
	}
	
	
}

if(isset($_POST['accepted'])){
	
	$comment = "";
	$score = $_POST['testscore'] . "/" . $_POST['testoverall'];
	$the_score = str_replace('/', '', $score);
	if(trim($_POST['acceptcomment']) == NULL){
		$comment = "<p class = 'redtext'>No comment</p>";
	}
	else{
		$comment = "<p>".$_POST['acceptcomment']."</p>";
	}
	if(trim($the_score) == NULL){
		$score = "<p class = 'straight redtext'>N/A</p>";
	}
	else{
		$score = "<p class = 'straight'>".$score."</p>";
	}

	if($app_data['applicant_phase'] == $position_data['set_type_rounds']){
		if(strcmp($app_data['applicant_status'], "For Interview") == 0){
		$appcomment = $app_data['applicant_comment'] . "<p><strong><u>Remarks (".$curr_date."):</u></strong></p>" .$comment;
		$applicant_obj = array(
								'applicant_status'	=> "Successful",
								'applicant_read'	=> 1,
								'applicant_comment' => $appcomment,
								'applicant_datehired' => $curr_date
								);
	
		$applicant_filter[] = array('applicant_id=%d', $_POST['id']);
		if($database->updateRows('navy_applicant', $applicant_obj, $applicant_filter)){
			$app_log = get_applicantData($_POST['id']);
			$appid = $app_log['applicant_id'];
			$appname = $app_log['applicant_first_name']." ".$app_log['applicant_last_name'];
			$appstatus = $app_log['applicant_status'];
			$app_phase = $app_log['applicant_phase'];

			$log_params = array($app_log['applicant_status'], $app_log['applicant_id'], $appname, $app_log['applicant_phase'], $user_fullname, $pos_desc, $curr_date);

			$log_obj = array(
					'log_user' => $user_id,
					'log_action' => get_applicantlog($log_params),
					'log_target' => $app_log['applicant_id'],
					'log_date' => $curr_date,
					'log_type' => 'applicant',
					'log_company' => 1
				);
			$database->insertRow('navy_logs', $log_obj);
		}
	
		}
		else{

			$appcomment = $app_data['applicant_comment'] . "<p><strong><u>Round ".$round." Review (".$curr_date."):</u></strong></p><p class = 'straight'><strong>Score: ".$score."</strong></p><p><strong>Rating:</strong></p><p><span class = 'stars mid'>".$_POST['rating']."</span></p><p><strong>Comment:</strong></p>".$comment;
			
			$answer_obj = array(
						'ans_rating' => $_POST['rating']
					);
			$answer_filter[] = array('ans_id=%d', $answer_id);
			$database->updateRows('navy_answer', $answer_obj, $answer_filter);

			$applicant_obj = array(
									'applicant_status'	=> "For Interview",
									'applicant_read'	=> 1,
									'applicant_rating'  => $_POST['rating'],
									'applicant_comment' => $appcomment
									);
		
			$applicant_filter[] = array('applicant_id=%d', $_POST['id']);
			if($database->updateRows('navy_applicant', $applicant_obj, $applicant_filter)){
				$mail = array($app_data['applicant_email'],$position_data['set_type_desc'],$app_data['applicant_first_name']);
				send_interviewMail($mail);

				$app_log = get_applicantData($_POST['id']);
				$appid = $app_log['applicant_id'];
				$appname = $app_log['applicant_first_name']." ".$app_log['applicant_last_name'];
				$appstatus = $app_log['applicant_status'];
				$app_phase = $app_log['applicant_phase'];

				$log_params = array($app_log['applicant_status'], $app_log['applicant_id'], $appname, $app_log['applicant_phase'], $user_fullname, $pos_desc, $curr_date);

				$log_obj = array(
						'log_user' => $user_id,
						'log_action' => get_applicantlog($log_params),
						'log_target' => $app_log['applicant_id'],
						'log_date' => $curr_date,
						'log_type' => 'applicant',
						'log_company' => 1
					);
				$database->insertRow('navy_logs', $log_obj);
			}	
		}
		
	}
	else{
		if(strcmp($app_data['applicant_status'], "Unsuccessful") == 0){
			$appcomment = $app_data['applicant_comment'] . "<p><strong><u>Round ".$round." Reason for Another Chance (".$curr_date.")</u></strong></p><p class = 'straight'><strong>Score: ".$score."</strong></p><p><strong>Rating:</strong></p><p><span class = 'stars mid'>".$_POST['rating']."</span></p><p><strong>Comment:</strong></p>" .$comment;
		}
		else{
			$appcomment = $app_data['applicant_comment'] . "<p><strong><u>Round ".$round." Review (".$curr_date."):</u></strong></p><p class = 'straight'><strong>Score: ".$score."</strong></p><p><strong>Rating:</strong></p><p><span class = 'stars mid'>".$_POST['rating']."</span></p><p><strong>Comment:</strong></p>" .$comment;
		}
		
		$answer_obj = array(
						'ans_rating' => $_POST['rating']
					);
		$answer_filter[] = array('ans_id=%d', $answer_id);
		$database->updateRows('navy_answer', $answer_obj, $answer_filter);
		$applicant_obj = array(
							'applicant_status'	=> "For Stage ".($app_data['applicant_phase']+1),
							'applicant_phase'	=> ($app_data['applicant_phase']+1),
							'applicant_read'	=> 1,
							'applicant_rating'  => $_POST['rating'],
							'applicant_comment' => $appcomment
						);
	
		$applicant_filter[] = array('applicant_id=%d', $_POST['id']);
		if($database->updateRows('navy_applicant', $applicant_obj, $applicant_filter)){
				$mail = array(
					$app_data['applicant_email'],
					$position_data['set_type_desc'],
					$app_data['applicant_first_name'],
					get_nextStepLink($_POST['id'],$app_data['applicant_position'])
					);
				send_successMail($mail);

				$app_log = get_applicantData($_POST['id']);
				$appid = $app_log['applicant_id'];
				$appname = $app_log['applicant_first_name']." ".$app_log['applicant_last_name'];
				$appstatus = $app_log['applicant_status'];
				$app_phase = $app_log['applicant_phase'];

				$log_params = array($app_log['applicant_status'], $app_log['applicant_id'], $appname, $app_log['applicant_phase'], $user_fullname, $pos_desc, $curr_date);

				$log_obj = array(
						'log_user' => $user_id,
						'log_action' => get_applicantlog($log_params),
						'log_target' => $app_log['applicant_id'],
						'log_date' => $curr_date,
						'log_type' => 'applicant',
						'log_company' => 1
					);
				$database->insertRow('navy_logs', $log_obj);
		}
	
		
	}
	

	


}

header("Location:".$_POST['ref']);
exit;
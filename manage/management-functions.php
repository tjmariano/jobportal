<?php

function print_hiredapplicantTable($applicants,$reference){

echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Position</th>
				<th>Status</th>
				<th>Rating / Comment</th>
				<th>Date Hired</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($applicants as $applicant){
		echo "
			<tr>
				<td><a href='applicant.php?app=".$applicant['applicant_id']."' target='_blank'>".$applicant['applicant_first_name']." ".$applicant['applicant_last_name']."</a></td>
				<td>".$applicant['applicant_email']."</td>
				<td>".$applicant['applicant_contact']."</td>
				<td>".get_positionDescription($applicant['applicant_position'])."</td>
				<td>".$applicant['applicant_status']."</td>
				<td style = 'text-align:center;vertical-align:middle;'>".get_apprating($applicant['applicant_rating'])." / <button class = 'btn btn-link' role = 'button' data-toggle='modal' data-target='#appcommentModal' onclick = '$.fn.appcomm(\"".$applicant['applicant_id']."\")'>Show</button></td>
				<td>".$applicant['applicant_datehired']."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}

function print_newapplicantTable($applicants,$reference){
echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Position</th>
				<th>Status</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($applicants as $applicant){
		$set_dateans = get_ansdate($applicant['applicant_id']);
		echo "
			<tr>
				<td><a href='applicant.php?app=".$applicant['applicant_id']."' target='_blank'>".$applicant['applicant_first_name']." ".$applicant['applicant_last_name']."</a></td>
				<td>".$applicant['applicant_email']."</td>
				<td>".$applicant['applicant_contact']."</td>
				<td>".get_positionDescription($applicant['applicant_position'])."</td>
				<td>".$applicant['applicant_status']."</td>
				<td>".$set_dateans['ans_date_taken']."</td>
				<td>".get_actionButtons($applicant['applicant_id'],$reference)."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}

function print_applicantTable($applicants,$reference){
echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Position</th>
				<th>Status</th>
				<th>Rating / Comment</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($applicants as $applicant){
		$set_dateans = get_ansdate($applicant['applicant_id']);
		echo "
			<tr>
				<td><a href='applicant.php?app=".$applicant['applicant_id']."' target='_blank'>".$applicant['applicant_first_name']." ".$applicant['applicant_last_name']."</a></td>
				<td>".$applicant['applicant_email']."</td>
				<td>".$applicant['applicant_contact']."</td>
				<td>".get_positionDescription($applicant['applicant_position'])."</td>
				<td>".$applicant['applicant_status']."</td>
				<td style = 'text-align:center;vertical-align:middle;'>".get_apprating($applicant['applicant_rating'])." / <button class = 'btn btn-link' role = 'button' data-toggle='modal' data-target='#appcommentModal' onclick = '$.fn.appcomm(\"".$applicant['applicant_id']."\")'>Show</button></td>
				<td>".$set_dateans['ans_date_taken']."</td>
				<td>".get_actionButtons($applicant['applicant_id'],$reference)."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}
function print_stageTable($applicants,$reference){
echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Position</th>
				<th>Status</th>
				<th>Rating / Comment</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($applicants as $applicant){
		$set_dateans = get_ansdate($applicant['applicant_id']);
		echo "
			<tr>
				<td><a href='applicant.php?app=".$applicant['applicant_id']."' target='_blank'>".$applicant['applicant_first_name']." ".$applicant['applicant_last_name']."</a></td>
				<td>".$applicant['applicant_email']."</td>
				<td>".$applicant['applicant_contact']."</td>
				<td>".get_positionDescription($applicant['applicant_position'])."</td>
				<td>".$applicant['applicant_status']."</td>
				<td style = 'text-align:center;vertical-align:middle;'>".get_apprating($applicant['applicant_rating'])." / <button class = 'btn btn-link' role = 'button' data-toggle='modal' data-target='#appcommentModal' onclick = '$.fn.appcomm(\"".$applicant['applicant_id']."\")'>Show</button></td>
				<td>".$set_dateans['ans_date_taken']."</td>
				<td>Resend <br/>".get_ResendButton($applicant['applicant_id'],$reference)."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}
function print_rejectTable($applicants,$reference){
echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Contact Number</th>
				<th>Position</th>
				<th>Status</th>
				<th>Reason</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($applicants as $applicant){

		$set_dateans = get_ansdate($applicant['applicant_id']);
		echo "
			<tr>
				<td><a href='applicant.php?app=".$applicant['applicant_id']."' target='_blank'>".$applicant['applicant_first_name']." ".$applicant['applicant_last_name']."</a></td>
				<td>".$applicant['applicant_email']."</td>
				<td>".$applicant['applicant_contact']."</td>
				<td>".get_positionDescription($applicant['applicant_position'])."</td>
				<td>".$applicant['applicant_status']."</td>
				<td><button class = 'btn btn-link' role = 'button' data-toggle='modal' data-target='#appcommentModal' onclick = '$.fn.appcomm(\"".$applicant['applicant_id']."\")'>Show</button></td>
				<td>".$set_dateans['ans_date_taken']."</td>
				<td>".get_acceptButton($applicant['applicant_id'],$reference)."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}
function print_logsdropdown($logdata){

	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');
	
	$count = count($logdata);

	echo 			"<li>
                    <a href = '#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                       Logs ".get_countlogs($count)."<span class='caret'></span>
                    </a>
                    <ul class='dropdown-menu dropdown-hover dropdown-logs' role='menu'>
                    <li class = 'center dropdown-footer'><a href='#' data-toggle='modal' data-target='#showLogsModal'>Show All Logs <br/><p class = 'green'>".$count." unread logs</p></a></li>
                    <li class='divider'></li>";
                    	if($count == '0'){
                    		echo "<li class = 'center redtext'>No New Log</li>
                    				<li class='divider'></li>";
                    	}
                    	else{
		      				foreach($logdata as $log){
		      				$query = "SELECT * FROM navy_users WHERE user_company = 1 AND user_active = 1 AND user_id = ".$log['log_user'];
  							$user_data = $database->query($query);
  							$fullname = $user_data[0]['user_fname']." ".$user_data[0]['user_lname'];
		                      echo "<li onclick = '$.fn.readlog(\"".$log['log_id']."\")'><a href = 'applicant.php?app=".$log['log_target']."'><strong>".$fullname."</strong>".$log['log_action']."<span class='date-display'>".$log['log_date']."</span></a></li>
		                      		<li class='divider'></li>";
		                  	}
	                  	}
    echo            "</ul>
                    </li>";
}
function get_countlogs($count){
	if ($count == 0){
		return "";
	}
	else{
		return "(".$count.")";
	}
}
function get_logtype($obj){
	if($obj[0] == 'applicant'){
		echo "<a href = 'applicant.php?app=".$obj[3]."'><strong>".$obj[1]."</strong>".$obj[2]."</a>";
	}
	elseif($obj[0] == 'user'){
		echo "<strong>".$obj[1]."</strong>".$obj[2];
	}
}
function print_accountdropdown($user, $uri, $pass){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT * FROM navy_users WHERE user_username = '$user'";
  	$user_data = $database->query($query);

  	$user_id = $user_data[0]['user_id'];
	echo
					"<li>
                    <a href = '#' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                       Hi ".$user_data[0]['user_lname'].", ".$user_data[0]['user_fname']."<span class='caret'></span>
                    </a>
                    <ul class='dropdown-menu dropdown-hover dropdown-account' role='menu'>
                    	<li><a href = '#' role='button' data-toggle='modal' data-target='#acctSetModal' onclick = '$.fn.getUserset(\"".$user_id."\", \"".$uri."\")'>Account Settings</a></li>
                      <li><a href = '#' role='button' data-toggle='modal' data-target='#chgPassModal' onclick = '$.fn.getUser(\"".$user_id."\", \"".$uri."\", \"".$pass."\")'>Change Password</a></li>
                      <li class='divider'></li>
                      <li><a href='".get_home()."logout.php'>Logout</a></li>
                    </ul>
                    </li>";
}

function get_apprating($applicant_rating){
	if ($applicant_rating <= 2){
		return "<p class = 'straight redtext'>".$applicant_rating."</p>";
	}
	elseif($applicant_rating == 3){
		return "<p class = 'straight yellowtext'>".$applicant_rating."</p>";
	}
	else{
		return "<p class = 'straight greentext'>".$applicant_rating."</p>";
	}
}

function get_actionButtons($applicant,$reference){
return "

        <button class='btn btn-xs btn-success acceptapp' href='#' role='button' data-toggle='modal' data-target='#acceptAppModal' onclick = '$.fn.acceptapp(\"".$applicant."\", \"".$reference."\")'><span class='glyphicon glyphicon glyphicon-ok'></span></button>

    <p class = 'straight spacing'></p>
	
        <button class='btn btn-xs btn-danger rejectapp' href='#' role='button' data-toggle='modal' data-target='#rejectAppModal' onclick = '$.fn.rejectapp(\"".$applicant."\", \"".$reference."\")'><span class='glyphicon glyphicon glyphicon-remove'></span></button>
    
";	

}
function get_ResendButton($applicant,$reference){
return "
    <form action='ProcessResend.php' method='post' class = 'straight'>
        <input type='hidden' name='id' value='$applicant'/>
		<input type='hidden' name='ref' value='$reference'/>
        <button class='btn btn-xs btn-success' name='accepted' type='submit' onclick=\"return confirm('Do you want to resend email to the applicant?')\"><span class='glyphicon glyphicon glyphicon-ok'></span></button>
    </form>
    <p class = 'straight spacing'></p>
	
        <button class='btn btn-xs btn-danger rejectapp' href='#' role='button' data-toggle='modal' data-target='#rejectAppModal' onclick = '$.fn.rejectapp(\"".$applicant."\", \"".$reference."\")'><span class='glyphicon glyphicon glyphicon-remove'></span></button>
";	

}
function get_acceptButton($applicant,$reference){
return "
    <button class='btn btn-xs btn-success acceptapp' href='#' role='button' data-toggle='modal' data-target='#acceptAppModal' onclick = '$.fn.acceptapp(\"".$applicant."\", \"".$reference."\")'><span class='glyphicon glyphicon glyphicon-ok'></span></button>
";	

}
function get_applicantButtons($applicant, $reference, $status, $phase){
	if($status == 'For Stage '.$phase){
		echo "
				<div id = '#resem' class = 'straight right'>
				<form action='ProcessResend.php' method='post' class = 'straight'>
			        <input type='hidden' name='id' value='$applicant'/>
					<input type='hidden' name='ref' value='$reference'/>
			        <button class='btn btn-xs btn-success thefont' name='accepted' type='submit' onclick=\"return confirm('Do you want to resend email to the applicant?')\">RESEND EMAIL</button>
			    </form>

			    <button class='btn btn-xs btn-danger rejectapp thefont' href='#' role='button' data-toggle='modal' data-target='#rejectAppModal' onclick = '$.fn.rejectapp(\"".$applicant."\", \"".$reference."\")'>FAIL</button>
			    </div>
			";
	}
		
	elseif($status == 'Unsuccessful'){
		echo "
			<div class = 'straight right'>
		    <button class='btn btn-xs btn-success acceptapp thefont' href='#' role='button' data-toggle='modal' data-target='#acceptAppModal' onclick = '$.fn.acceptapp(\"".$applicant."\", \"".$reference."\")'>ANOTHER CHANCE</button>
		    </div>
		";
	}
	else{
			echo "
		    <div class = 'straight right'>
		        <button class='btn btn-xs btn-success acceptapp thefont' href='#' role='button' data-toggle='modal' data-target='#acceptAppModal' onclick = '$.fn.acceptapp(\"".$applicant."\", \"".$reference."\")'>PASS</button>

		        <button class='btn btn-xs btn-danger rejectapp thefont' href='#' role='button' data-toggle='modal' data-target='#rejectAppModal' onclick = '$.fn.rejectapp(\"".$applicant."\", \"".$reference."\")'>FAIL</button>

		    </div>
		";	
	}
}

function get_countapp($applicant){
	if($applicant == 0){
		return
		"";
	}
	else {
		return
		"<p class = 'straight count'><label style='display:none;'>(</label>" .$applicant. "<label style='display:none;'>)</label></p>";
	}
}
function get_countstageapp($applicant){
	if($applicant == 0){
		return
		"";
	}
	else {
		return
		"<p class = 'straight countyellow'><label style='display:none;'>(</label>" .$applicant. "<label style='display:none;'>)</label></p>";
	}
}
function get_countrejectapp($applicant){
	if($applicant == 0){
		return
		"";
	}
	else {
		return
		"<p class = 'straight countred'><label style='display:none;'>(</label>" .$applicant. "<label style='display:none;'>)</label></p>";
	}
}
function get_countapptitle($applicant){
	if($applicant == 0){
		return
		"";
	}
	else {
		return
		"(" .$applicant. ")";
	}
}

function get_currentuser($user){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT * FROM navy_users WHERE user_username = '".$user."' AND user_company = 1 AND user_active = 1";
	$data = $database->query($query);

	return $data[0];
}

function get_applicantlog($log_params){
	if ($log_params[0] == "Unsuccessful"){
		return " <span class='straight'><img src = '../bootstrap/images/reject.png' /></span> has rejected <strong>".$log_params[2]." (".$log_params[5].")</strong> and failed <strong>Round ".$log_params[3]."</strong>.";
	}
	elseif($log_params[0] == "For Stage ".$log_params[3]){
		return " <span class='straight'><img src = '../bootstrap/images/accept.png' /></span> passed <strong>".$log_params[2]." (".$log_params[5].")</strong> and has moved on to <strong>Round ".$log_params[3]."</strong>.";
	}
	elseif($log_params[0] == "For Interview"){
		return " <span class='straight'><img src = '../bootstrap/images/accept.png' /></span> passed <strong>".$log_params[2]." (".$log_params[5].")</strong> and is ready <strong>".$log_params[0]."</strong>.";
	}
	elseif($log_params[0] == "Successful"){
		return " <span class='straight'><img src = '../bootstrap/images/accept.png' /></span> hired <strong>".$log_params[2]." (".$log_params[5].")</strong>.";
	}
}

function get_ansdate($appid){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT ans_date_taken FROM navy_answer WHERE ans_applicant = ".$appid." ORDER BY ans_date_taken DESC LIMIT 1";
	$data = $database->query($query);

	return $data[0];
}

function get_positionDescription($id){
$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

$query = "SELECT set_type_desc FROM navy_set_type WHERE set_type_id = $id";
$data = $database->query($query);

return $data[0]['set_type_desc'];
}

function get_applicantData($id){
$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

$query = "SELECT * FROM navy_applicant WHERE applicant_id = $id";
$data = $database->query($query);

return $data[0];
}

function get_positionData($id){
$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

$query = "SELECT * FROM navy_set_type WHERE set_type_id = $id";
$data = $database->query($query);

return $data[0];
}

function get_questionset($round, $position){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT * FROM navy_set WHERE set_position = $position AND set_order = $round";
	$data = $database->query($query);

	return $data[0];
}

function get_answerforrating($appid, $setcode){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT * FROM navy_answer WHERE ans_set = '$setcode' AND ans_applicant = $appid";
	$data = $database->query($query);

	return $data[0];
}

function get_rateavg($appid){
$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

$query = "SELECT ans_rating FROM navy_answer where ans_applicant = $appid";
$rate_data = $database->query($query);

for($i = 0; $i < count($rate_data); $i++){
	$arr_ratings[] = $rate_data['ans_rating'];
}
$sum_ratings = array_sum($arr_ratings);
$count_ratings = count($arr_ratings);
$avg_ratings = $sum_ratings / $count_ratings;
return $avg_ratings;
}

function generatePassword($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function send_resetpassMail($obj){
	$to = $obj[0];
	$subject = "FourPoint.Zero, Inc. - Hiring App Reset Password";

	$message = "
		<html>
		<head>
		<title>FourPoint.Zero Hiring App Reset Password</title>
		</head>
		<body>
		<p>Hi ".$obj[1].",</p>
		<br/>
		<p>You're password has been reset.</p>
		<br/>
		<p>Password: ".$obj[2]."</p>
		<p>Please change your password immediately at ".$obj[3]."</p>
		<br/>
		<p>Regards,<p>
		<p>FourPoint.Zero, Inc.<p>
		<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
		</body>
		</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function send_resetpassMailtoAdmin($obj){
	$to = $obj[0];
	$subject = "FourPoint.Zero, Inc. - Hiring App Reset Password";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Hiring App Reset Password</title>
	</head>
	<body>
	<p>Hi Admin,</p>
	<br/>
	<p>Password Reset for ".$obj[1]." was successful and the new password has been sent to ".$obj[2]."</p>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function send_accountMailtoAdmin($obj){
	$to = $obj[0];
	$subject = "FourPoint.Zero, Inc. - Hiring App User Registration";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Hiring App User Registration</title>
	</head>
	<body>
	<p>Hi Admin,</p>
	<br/>
	<p>User Registration for ".$obj[1]." was successful and account details have been sent to ".$obj[2]."</p>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function send_accountMail($obj){
	$to = $obj[0];
	$subject = "FourPoint.Zero, Inc. - Hiring App User Registration";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Hiring App User Registration</title>
	</head>
	<body>
	<p>Hi ".$obj[1].",</p>
	<br/>
	<p>You have been registered as a user for the FPZ Hiring App</p>
	<br/>
	<p>Username: ".$obj[2]."</p>
	<p>Password: ".$obj[3]."</p>
	<p>Please change your password immediately at ".$obj[4]."</p>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}
function send_rejectionMail($obj){
	$to = $obj[0];
	$subject ="FourPoint.Zero, Inc. - ".$obj[1]." Application Update";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Application Update</title>
	</head>
	<body>
	<p>Hi ".$obj[2].",</p>
	<br/>
	<p>Thank you for your application for the position of ".$obj[1]." at FourPoint.Zero, Inc. As you can imagine, we received a large number of applications. We're sorry to inform you that you have not been selected for an interview for this position.</p>
	<br/>
	<p>The FourPoint.Zero team thanks you for the time you invested to apply for the position of ".$obj[1].". We encourage you to apply for future openings for which you qualify.</p>
	<br/>
	<p>We still think you're awesome!<p>
	<br/>
	<p>Best wishes for a successful job search. Thank you, again, for your interest in our company.<p>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}


function send_successMail($obj){
	$to = $obj[0];
	$subject ="FourPoint.Zero, Inc. - ".$obj[1]." Application Update";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Application Update</title>
	</head>
	<body>
	<p>Hi ".$obj[2]."!</p>
	<p>Congratulations! You've passed the screening.<p>
	<p>Click on the link below to proceed to the next step.<p>
	<br/>
	<a href='".$obj[3]."'' target='_blank'>".$obj[3]."</a>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function send_interviewMail($obj){
	$to = $obj[0];
	$subject ="FourPoint.Zero, Inc. - ".$obj[1]." Application Update";

	$message = "
	<html>
	<head>
	<title>FourPoint.Zero Application Update</title>
	</head>
	<body>
	<p>Hi ".$obj[2]."!</p>
	<p>Congratulations! You've passed the final screening.<p>
	<p>We'll contact you in a day or two to schedule you for an interview.<p>
	<br/>
	<p>Regards,<p>
	<p>FourPoint.Zero, Inc.<p>
	<p><a href='http://fourptzero.com'>www.fourptzero.com</a><p>
	</body>
	</html>
	";	

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <jobs@fourptzero.com>' . "\r\n";

	mail($to,$subject,$message,$headers);
}

function get_nextStepLink($app_id,$pos_id){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	$query = "SELECT * FROM navy_applicant WHERE applicant_id = $app_id";
	$app_data = $database->query($query);
	
	$query = "SELECT set_code FROM navy_set WHERE set_position = $pos_id AND set_order = ".$app_data[0]['applicant_phase'];
	$set_data = $database->query($query);

	$set = $set_data[0]['set_code'];
	$token = $app_data[0]['applicant_token'];
	$order = $app_data[0]['applicant_phase'];
	$position = $pos_id;
	
	return get_home()."position/application.php?s=$set&t=$token&o=$order&p=$position";
}


function print_questionnaireWithAnswers($set,$set_answers){
	$questions = json_decode($set,true);
	$answers = json_decode($set_answers,true);
	$question_order = 0;
	foreach($questions as $question){
		if($question['type'] === 'essay'){
			echo "
				<p class='text-muted'>".$question['order']. ") " . $question['question']."</p>
				<textarea class='form-control' rows='4' disabled>".$answers[$question_order][1]."</textarea>
				<br/>
			";
			
			$question_order++;
		}elseif($question['type'] === 'file'){
			echo "
			<p class='text-muted'>".$question['order']. ") " .$question['question']."</p>
			<a href='http://jobs.fourptzero.com/uploads/".$answers[$question_order][1]."' target='_blank'>View Answer</a>
			<br/>
			";
			$question_order++;
		}elseif($question['type'] === 'link'){
			echo "
			<p class='text-muted'>".$question['question']."</p>
			<a href='".$answers[$question_order][1]."' target='_blank'>".$answers[$question_order][1]."</a>
			<br/>
			";
			$question_order++;
		}elseif($question['type'] === 'field'){
			echo "
			<p class = 'text-muted'>Video Password: ".$answers[$question_order][1]."</p>
			<br/>
			";
			$question_order++;
		}elseif($question['type'] === 'image'){
			echo "
			<p class='text-muted'>".$question['order']. ") " .$question['question']."</p>
			<a href='http://jobs.fourptzero.com/uploads/".$answers[$question_order][1]."' target='_blank'>View Answer</a>
			<br/>
			";
			$question_order++;
		}

	}
	

	}

function print_userMaintTable($usermaint){
	echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Name</th>
				<th>Position</th>
				<th>Level</th>
				<th>Email Address</th>
				<th>Mail Notification</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($usermaint as $user){
		$user_fullname = $user['user_fname']." ".$user['user_lname'];
		echo "
			<tr>
				<td><a href = '#' data-toggle ='modal' data-target ='#editUserModal' onclick = '$.fn.getUserdetails(\"".$user['user_id']."\")'>".$user['user_lname'].", ".$user['user_fname']."</a></td>
				<td>".get_userposition($user['user_position'])."</td>
				<td>".$user['user_level']."</td>
				<td>".$user['user_email']."</td>
				<td>".get_mailnotif($user['user_setmail'], $user['user_id'], $user_fullname)."</td>
				<td>".get_useractive($user['user_active'], $user['user_id'])."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}

function print_posMaintTable($posmaint){
echo "  
	<table class='table table-bordered example'>
		<thead>
			<tr>
				<th>Position Name</th>
				<th>Position Slug</th>
				<th>Questionnaire Rounds</th>
				<th>Category</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
    ";
	foreach($posmaint as $pos){
		echo "
			<tr>
				<td><button class='btn btn-link editPos' href='#' role='button' data-toggle='modal' data-id = '".$pos['set_type_id']."' data-target='#editPosModal'>".$pos['set_type_desc']."</button></td>
				<td>".$pos['set_type_slug']."</td>
				<td><button class='btn btn-link' href='#' role='button' data-backdrop = 'static' data-keyboard = 'false' data-toggle='modal' data-target='#editQuestions' onclick = '$.fn.getQuestions(\"".$pos['set_type_id']."\")'>".$pos['set_type_rounds']."</button></td>
				<td>".get_postype($pos['set_postype'])."</td>
				<td>".get_active($pos['set_type_active'], $pos['set_type_id'])."</td>
			</tr>
		";
	}
echo "	
		</tbody>
	</table>
	";
}
function get_userposition($id){
	$database = eden('mysql', 'db.fourptzero.com' ,'tamayojp_hyred', 'tamayojp_db', 'S47ZB9K8k');

	if ($id == 'admin'){
		return 'admin';
	}
	else{

	$query = "SELECT * FROM navy_set_type where set_type_id = ".$id;
	$data = $database->query($query);

	return $data[0]['set_type_desc'];
	}
}
function get_useractive($status, $userid){
	if($status === '0'){
		return "<form action='process-usermaint.php' method='post'>
        <input type='hidden' name='id' value='$userid'/>
        <button name='setactive' type='submit' class='btn btn-default' onclick=\"return confirm('Do you want to activate this user?')\">Inactive</button>
    	</form>";
	}
	elseif($status === '1'){
		return "<form action='process-usermaint.php' method='post'>
        <input type='hidden' name='id' value='$userid'/>
        <button name='setactive' type='submit' class='btn btn-default' onclick=\"return confirm('Do you want to deactivate this user?')\">Active</button>
    	</form>";
	}
}
function get_mailnotif($setmail, $userid, $user_fullname){
	if($setmail === '1'){
		return "<form action='process-usermaint.php' method='post'>
        <input type='hidden' name='setmail_id' value='$userid'/>
        <button name='submit_setmail' type='submit' class='btn btn-default' onclick=\"return confirm('Are you sure you want to deactivate mail notifications for ".$user_fullname."?')\">YES</button>
    	</form>";
	}
	elseif($setmail === '0'){
		return "<form action='process-usermaint.php' method='post'>
        <input type='hidden' name='setmail_id' value='$userid'/>
        <button name='submit_setmail' type='submit' class='btn btn-default' onclick=\"return confirm('Are you sure you want to activate mail notifications for ".$user_fullname."?')\">NO</button>
    	</form>";
	}
}
function get_postype($postype){
	if ($postype === '1'){
		return "Intern";
	}
	elseif ($postype === '2'){
		return "Regular";
	}
}


function get_active($state, $position_id){
	if ($state === '0'){
		return "<form action='pos-processstatus.php' method='post'>
        <input type='hidden' name='id' value='$position_id'/>
        <button name='grant' type='submit' class='btn btn-default' onclick=\"return confirm('Do you want to activate this position?')\">Inactive</button>
    	</form>";
	}
	elseif ($state === '1'){
		return "<form action='pos-processstatus.php' method='post'>
        <input type='hidden' name='id' value='$position_id'/>
        <button name='grant' type='submit' class='btn btn-default' onclick=\"return confirm('Do you want to deactivate this position?')\">Active</button>
    	</form>";
	}
}
function print_tabapplicants($class, $tabName, $roundCtr, $phase, $status){

	if ($phase > $roundCtr) {
		echo

		"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'green straight'>PASS</p></a></li>";
	}
	elseif ($phase == $roundCtr){
		if($status == "For Interview" || $status == "Successful"){
			echo

			"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'green straight'>PASS</p></a></li>";
		}
		elseif($status == "For Stage ".$phase." Review"){
			echo

			"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'yellowtext straight'>REVIEW</p></a></li>";
		}
		elseif($status == "New Applicant"){
			echo

			"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'yellowtext straight'>REVIEW</p></a></li>";
		}
		else{

			echo

			"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'blue straight'>PENDING</p></a></li>";
		}
	}
	else {
		echo

		"<li ".$class."><a href='#pane".$roundCtr."' data-toggle='tab'>".$tabName." <p class = 'blue straight'>PENDING</p></a></li>";
	}
		
}


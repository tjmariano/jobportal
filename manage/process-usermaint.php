<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");
session_start();
check_login($_SESSION['username']);

//PHP POST FORMS START
if(isset($_POST['setactive'])){
	$loc = "/manage/user-maint.php";
	$userid = $_POST['id'];

	$query = "SELECT * FROM navy_users WHERE user_id = ".$userid;
	$user_data = $database->query($query);

	$user_status = $user_data[0]['user_active'];
	if($user_status === '0'){
		$user_obj = array('user_active' => 1);

		$user_filter[] = array('user_id=%d', $userid);
		$database->updateRows('navy_users', $user_obj, $user_filter);
	}
	elseif($user_status === '1'){
		$user_obj = array('user_active' => 0);

		$user_filter[] = array('user_id=%d', $userid);
		$database->updateRows('navy_users', $user_obj, $user_filter);
	}
	header("Location: $loc");
	exit;
}


elseif(isset($_POST['submit_setmail'])){
	$loc = "/manage/user-maint.php";
	$userid = $_POST['setmail_id'];

	$query = "SELECT * FROM navy_users WHERE user_id = ".$userid;
	$user_data = $database->query($query);

	$user_setmail = $user_data[0]['user_setmail'];
	if($user_setmail === '0'){
		$user_obj = array('user_setmail' => 1);

		$user_filter[] = array('user_id=%d', $userid);
		$database->updateRows('navy_users', $user_obj, $user_filter);
	}
	elseif($user_setmail === '1'){
		$user_obj = array('user_setmail' => 0);

		$user_filter[] = array('user_id=%d', $userid);
		$database->updateRows('navy_users', $user_obj, $user_filter);
	}
	header("Location: $loc");
	exit;
}


elseif(isset($_POST['submit_adduser'])){
	$loc = get_home()."/manage/user-maint.php";

	$username = $_POST['userLname'].".".$_POST['userFname'];
	$username = strtolower(preg_replace('/\s+/', '', $username));
	$length = 8;
	$password = generatePassword($length);
	$login_link = "<a href = ".get_login().">".get_login()."</a>";


	$parsed_password = hashpass($password);

	$user_obj = array(
			'user_lname' => $_POST['userLname'],
			'user_fname' => $_POST['userFname'],
			'user_position' => $_POST['userPos'],
			'user_level' => $_POST['userLevel'],
			'user_email' => $_POST['userEmail'],
			'user_setmail' => $_POST['userSetmail'],
			'user_username' => $username,
			'user_password' => $parsed_password,
			'user_company' => 1
		);
	if($database->insertRow('navy_users', $user_obj)){

		$admin_email = "jobs@fourptzero.com, prince.ryan.sy@gmail.com";
		$user_fullname = $_POST['userLname'].", ".$_POST['userFname'];

		$mail = array($_POST['userEmail'], $_POST['userFname'], $username, $password, $login_link);
		send_accountMail($mail);
		$mail2 = array($admin_email, $user_fullname, $_POST['userEmail']);
		send_accountMailtoAdmin($mail2);

		header("Location: $loc");
		exit;
	}
}

elseif(isset($_POST['submit_edituser'])){
	$userid = $_POST['edituserid'];
	$loc = "/manage/user-maint.php";

	$user_obj = array(
			'user_lname' => $_POST['edituserLname'],
			'user_fname' => $_POST['edituserFname'],
			'user_position' => $_POST['edituserPos'],
			'user_level' => $_POST['edituserLevel'],
			'user_email' => $_POST['edituserEmail']
		);

	$user_filter[] = array('user_id=%d', $userid);
	$database->updateRows('navy_users', $user_obj, $user_filter);

	header("Location: $loc");
	exit;
}


elseif(isset($_POST['submit_chgPassword'])){
	$userid = $_POST['chg_userid'];
	$uri = $_POST['chg_useruri'];
	$newpass = $_POST['txtConfirmPass'];
	$parsednewpass = hashpass($newpass);

	$loc = get_home().$uri;

	$query = "SELECT * FROM navy_users WHERE user_id = ".$userid." AND user_active = 1 AND user_company = 1";
	$user_data = $database->query($query);

	$iduser = $user_data[0]['user_id'];

	$chgpass_obj = array(
			'user_password' => $parsednewpass
		);
	$chgpass_filter[] = array('user_id=%d', $iduser);
	if($database->updateRows('navy_users', $chgpass_obj, $chgpass_filter)){

			$_SESSION['password'] = $newpass;
			header("Location: $uri");
			exit;	
	}
}
elseif(isset($_POST['submit_acctset'])){
	$userid = $_POST['acct_userid'];
	$uri = $_POST['acct_useruri'];



	$user_obj = array(
			'user_lname' => $_POST['txtLname'],
			'user_fname' => $_POST['txtFname'],
			'user_email' => $_POST['txtEmail'],
			'user_setmail' => $_POST['setmail']
		);

	$user_filter[] = array('user_id=%d', $userid);
	$database->updateRows('navy_users', $user_obj, $user_filter);
	header("Location: $uri");
	exit;	
}
//PHP POST FORMS END

//AJAX POST START
elseif(isset($_POST['submit_resetuser'])){
	$userid = $_POST['edituserid'];
	$uri = "/manage/user-maint.php";

	$query = "SELECT * FROM navy_users WHERE user_id = ".$userid;
	$user_data = $database->query($query);

	$length = 8;
	$password = generatePassword($length);
	$login_link = "<a href = ".get_login().">".get_login()."</a>";


	$parsed_password = hashpass($password);

	$resetpass_obj = array(
			'user_password' => $parsed_password
		);

	$resetpass_filter[] = array('user_id=%d', $userid);
	if($database->updateRows('navy_users', $resetpass_obj, $resetpass_filter)){
		$admin_email = "jobs@fourptzero.com, prince.ryan.sy@gmail.com";
		$user_fullname = $user_data[0]['user_fname']." ".$user_data[0]['user_lname'];

		$mail = array($user_data[0]['user_email'], $user_data[0]['user_fname'], $password, $login_link);
		send_resetpassMail($mail);
		$mail2 = array($admin_email, $user_fullname, $user_data[0]['user_email']);
		send_resetpassMailtoAdmin($mail2);
		header("Location: $uri");
		exit;
	}
}

elseif(isset($_POST['userset_id'])){
	$userid = $_POST['userset_id'];
	$uri = $_POST['userset_uri'];

	$query = "SELECT * FROM navy_users where user_id = ".$userid;
	$user_data = $database->query($query);

	

	$user_position = "";

	if($user_data[0]['user_position'] == "admin"){
		$user_position = "admin";
	}
	else{
		$query = "SELECT * FROM navy_set_type where set_type_id =".$user_data[0]['user_position'];
		$pos_data = $database->query($query);
		
		$user_position = $pos_data[0]['set_type_desc'];
	}

	$acct_settings = array(
			'userid' => $user_data[0]['user_id'],
			'userLname' => $user_data[0]['user_lname'],
			'userFname' => $user_data[0]['user_fname'],
			'userPos' => $user_position,
			'userLevel' => $user_data[0]['user_level'],
			'userEmail' => $user_data[0]['user_email'],
			'userSetmail' => $user_data[0]['user_setmail'],
			'theuseruri' => $uri
		);

	echo json_encode($acct_settings);
	
}

elseif(isset($_POST['userdetail_id'])){
	$userid = $_POST['userdetail_id'];

	$query = "SELECT * FROM navy_users where user_id = ".$userid;
	$user_data = $database->query($query);

	echo json_encode($user_data);
}


elseif(isset($_POST['pass_user_id'])){
	$user = $_POST['pass_user_id'];
	$pass = $_POST['pass_user_pass'];
	$uri = $_POST['pass_user_uri'];

	$query = "SELECT * FROM navy_users WHERE user_id = '$user'";
	$user_data = $database->query($query);

	$user_array = array(
		'user_id' => $user_data[0]['user_id'],
		'username' => $user_data[0]['user_username'],
		'parsedpassword' => $user_data[0]['user_password'],
		'useruniquecode' => $pass,
		'the_uri' => $uri
		);
	echo json_encode($user_array);
}

//AJAX POST END
?>
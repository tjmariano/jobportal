<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");
session_start();

$the_user = $_SESSION['username'];
$the_pass = $_SESSION['password'];
check_login($_SESSION['username']);

$current_uri = $_SERVER['REQUEST_URI'];

$query = "SELECT * FROM navy_set_answer WHERE seta_company = 1";
$data = $database->query($query);

$query = "SELECT set_type_id,set_type_desc FROM navy_set_type WHERE set_type_company = 1 AND set_type_active = 1 ORDER BY set_type_desc ASC";
$positions = $database->query($query);

$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_read = 0";
$count_applicants = $database->query($query);

$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_status = 'New Applicant' AND applicant_read = 0";
$count_newapplicants = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_status = 'For Interview'";
$interview_applicants = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_status = 'Unsuccessful'";
$unsuccessful_applicants = $database->query($query);

$query = "SELECT * FROM navy_logs WHERE log_type = 'applicant' AND log_read = 0 ORDER BY log_date DESC";
$log_data = $database->query($query);

$query = "SELECT * FROM navy_users WHERE user_username = '".$the_user."' AND user_company = 1 AND user_active = 1";
$users = $database->query($query);


$interview_count = count($interview_applicants);
$unsuccessful_count = count($unsuccessful_applicants);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title><?php echo get_countapptitle($count_applicants[0]['apps']);?> FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	
	<link href="/bootstrap/css/dashboard.css" rel="stylesheet">
  <link href="/bootstrap/css/cssmenu.css" rel="stylesheet">
  <link rel="stylesheet" href="../bootstrap/formvalidation/css/formValidation.min.css">
  <link href="/bootstrap/css/jquery.circliful.css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="stylesheet" href="/bootstrap/css/datatables.css" />
  <link type="text/css" rel="stylesheet" href="/bootstrap/css/star-rating.css" />

   
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_admin();?>">FourPointZero</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
                    <?php print_logsdropdown($log_data);?>
                    <?php print_accountdropdown($the_user, $current_uri, $the_pass);?>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
		<div class="row">
			<div class="col-md-2 categories-sidebar">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title"><a href = "<?php echo get_admin();?>">All Applications <?php echo get_countapp($count_applicants[0]['apps']);?></a></h5>
                    </div>
					<div class="panel-body">
						<ul>
						<?php
							foreach($positions as $position){
                $query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_position = ".$position['set_type_id']." AND applicant_read = 0";
                $count_posapplicants = $database->query($query);
								echo "<li><a href='position.php?p=".$position['set_type_id']."'>".$position['set_type_desc']." ".get_countapp($count_posapplicants[0]['apps'])."</a></li>";
							}
						if ($users[0]['user_level'] == "admin"){ ?>
              <li><a href = "<?php echo get_home();?>manage/pos-maint.php">Positions</a></li>
              <li><a href = "<?php echo get_home();?>manage/user-maint.php">Users</a></li>
              <li><a class="btn btn-link" href="#" role="button" data-toggle="modal"  data-target="#addPosModal">+ Add New Position</a><li>
            <?php }
            else{

            }?> 

            
						</ul>
					</div>	
				</div>
			</div>
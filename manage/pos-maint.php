<?php
require_once("header.php");

$query = "SELECT count('set_type_id') AS result FROM navy_set_type WHERE set_type_active = 0";
$count_inactive = $database->query($query);

$query = "SELECT count('set_type_id') AS result FROM navy_set_type WHERE set_type_active = 1";
$count_active = $database->query($query);

$active = $count_active[0]['result'];
$inactive = $count_inactive[0]['result'];

?>

<div class="col-md-10 main">
	<h1 class="page-header">Positions</h1>
	<h2 class="sub-header"><?php echo $active?> Active / <?php echo $inactive?> Inactive</h2>
		<div class = "tabbable">
			<ul class = "nav nav-tabs">
				<li class = "active"><a href = "#tab1" data-toggle = "tab">Active</a></li>
				<li><a href = "#tab2" data-toggle = "tab">Inactive</a></li>
			</ul>
			<br/>
				<div class = "tab-content">
					<div id = "tab1" class = "tab-pane active">
						<?php 
							$query = "SELECT * FROM navy_set_type WHERE set_type_active = 1";
							$pos_maint = $database->query($query);
							print_posMaintTable($pos_maint);?>
					</div>
					<div id = "tab2" class = "tab-pane">
						<?php 
							$query = "SELECT * FROM navy_set_type WHERE set_type_active = 0";
							$pos_maint = $database->query($query);
							print_posMaintTable($pos_maint);?>
					</div>
				</div>
		</div>



</div>




<?php require_once("footer.php");?>
<?php require_once("pos-modal.php");?>
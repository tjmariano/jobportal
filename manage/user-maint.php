<?php
require_once("header.php");

$query = "SELECT * FROM navy_users where user_active = 1 AND user_company = 1";
$users_active = $database->query($query);

$query = "SELECT * FROM navy_users where user_active = 0 AND user_company = 1";
$users_inactive = $database->query($query);

$active = count($users_active);
$inactive = count($user_inactive);

?>

<div class="col-md-10 main">
	<h1 class="page-header">Users</h1>
	<div class="sub-header">
		<h2 class = "straight"><?php echo $active?> Active / <?php echo $inactive?> Inactive</h2>
		<a class="btn btn-primary straight right" href="#" role="button" data-toggle="modal"  data-target="#addUserModal">+ Add New User</a>
	</div>
		<div class = "tabbable">
			<ul class = "nav nav-tabs">
				<li class = "active"><a href = "#tab1" data-toggle = "tab">Active</a></li>
				<li><a href = "#tab2" data-toggle = "tab">Inactive</a></li>
			</ul>
			<br/>
				<div class = "tab-content">
					<div id = "tab1" class = "tab-pane active">
						<?php 
							print_userMaintTable($users_active);?>
					</div>
					<div id = "tab2" class = "tab-pane">
						<?php 
							print_userMaintTable($users_inactive);?>
					</div>
				</div>
		</div>



</div>

<?php require_once("footer.php");?>
<?php require_once("pos-modal.php");?>
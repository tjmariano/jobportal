<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
include "$root/ignition.php";
session_start();

check_login($_SESSION['username']);

$id = $_GET['id'];

$query = "SELECT set_content FROM navy_set WHERE set_company = 1 AND set_code = 'FPZ001'";
$set_data = $database->query($query);

$query = "SELECT * FROM navy_set_answer WHERE seta_id = ".$id."";
$data = $database->query($query);

$query = "SELECT * FROM navy_set_type WHERE set_type_id = ".$data[0]['seta_type']."";
$pos_data = $database->query($query);

$query = "SELECT * FROM navy_set_status WHERE set_status_id = ".$data[0]['seta_status']."";
$status_data = $database->query($query);

$applicant_answers = json_decode($data[0]['seta_answer'],true);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title>FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
  <link href="/bootstrap/css/jumbotron.css" rel="stylesheet">
  <link href="/bootstrap/css/dashboard.css" rel="stylesheet">
  <link href="/bootstrap/css/cssmenu.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo get_admin();?>">FourPointZero</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="<?php echo get_home();?>logout.php">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href = "#"><?php echo $pos_data[0]['set_type_desc']?> <span class="sr-only">(current)</span></a></li>
            <li class = "details">Name:<br/> <?php echo $data[0]['seta_first_name']." ".$data[0]['seta_last_name'];?></li>
            <li class = "details">Email:<br/> <?php echo $data[0]['seta_email'];?></li>
            <li class = "details">Contact Number:<br/> <?php echo $data[0]['seta_contact'];?></li>
            <li class = "details">Date Taken:<br/> <?php echo $data[0]['seta_date_taken'];?></li>
            <li class = "details"><a href="<?php echo get_home();?>uploads/<?php echo $data[0]['seta_id'];?>.pdf" target="_blank">Open Resume</a></li>
          </ul>
        </div>
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"><?php echo $data[0]['seta_first_name']. " ".$data[0]['seta_last_name'];?></h1>
          <h2 class="sub-header">Status: <?php echo $status_data[0]['set_status_desc']?></h2>
		  <?php
			$questions = json_decode($set_data[0]['set_content'],true);
			$question_order = 0;
			foreach($questions as $question){
			?>
			
					
					<p class="text-muted"><?php echo $question['question'];?></p>
					<textarea class="form-control" rows="4" disabled><?php echo $applicant_answers[$question_order][1];?></textarea>
					<br/>
			<?php		
				$question_order++;
			}
			?>
        </div>
       </div>
      </div>

      <hr>

      <footer>
        <p>&copy; FourPoint.Zero, Inc. 2015</p>
      </footer>
    </div> <!-- /container -->
	
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>	
  </body>
</html>



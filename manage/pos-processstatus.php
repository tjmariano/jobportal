<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");

if(isset($_POST['grant'])){
	$loc = get_home()."/manage/pos-maint.php";
	$pos_data = get_positiondata($_POST['id']);

	if($pos_data['set_type_active'] === '0'){
		$pos_obj = array('set_type_active'	=> 1);
	
		$pos_filter[] = array('set_type_id=%d', $_POST['id']);
		$database->updateRows('navy_set_type', $pos_obj, $pos_filter);
	}
	elseif($pos_data['set_type_active'] === '1'){
		$pos_obj = array('set_type_active'	=> 0);
	
		$pos_filter[] = array('set_type_id=%d', $_POST['id']);
		$database->updateRows('navy_set_type', $pos_obj, $pos_filter);
	}
	header("Location: $loc");
}
?>
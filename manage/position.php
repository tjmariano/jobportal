<?php
require_once("header.php");

$pid = $_GET['p'];
$reference = "position.php?p=$pid";

$query = "SELECT * FROM navy_set_type WHERE set_type_id = $pid";
$position_data = $database->query($query);

$rounds = $position_data[0]['set_type_rounds'];

$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid  AND applicant_status = 'For Interview'";
$interview_applicants = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid  AND applicant_status = 'Unsuccessful'";
$unsuccessful_applicants = $database->query($query);

$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_position = $pid AND applicant_read = 0";
$count_posapps = $database->query($query);

$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid AND applicant_status LIKE 'For Stage _'";
$pending_answers = $database->query($query);

$answers_count = count($pending_answers);

$unsuccessful_countpos = count($unsuccessful_applicants);
$interview_countpos = count($interview_applicants);

?>
	<div class="col-md-10 main">
        <h1 class="page-header"><?php echo $position_data[0]['set_type_desc'];?></h1>
			<h2 class="sub-header"><?php echo $count_posapps[0]['apps']?> Pending Application(s) / <?php echo $answers_count;?> Pending Answer(s)</h2>
            <div class="tabbable">
			  <ul class="nav nav-tabs">
				<?php
				for($roundCtr = 1; $roundCtr <= $rounds; $roundCtr++){
					$class = "";
					$tabName = "Stage $roundCtr";
					if($roundCtr == 1){
						$class = "class='active'";
						$tabName = "New Applicants";
					$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_position = $pid AND applicant_status = 'New Applicant' AND applicant_read = 0";
					$count_posnewapplicants = $database->query($query);
				?> 
					<li <?php echo $class;?>><a href="#pane<?php echo $roundCtr;?>" data-toggle="tab"><?php echo $tabName;?> <?php echo get_countapp($count_posnewapplicants[0]['apps']);?></a></li>
				<?php
					}
					else{
						$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_position = $pid AND applicant_status = 'For $tabName Review' AND applicant_read = 0";
						$count_applicants = $database->query($query);

						$query = "SELECT count(*) AS apps FROM navy_applicant WHERE applicant_position = $pid AND applicant_status = 'For $tabName' AND applicant_read = 1";
						$count_ansapp = $database->query($query);
				?>
					<li <?php echo $class;?>><a href="#pane<?php echo $roundCtr;?>" data-toggle="tab"><?php echo $tabName;?> (Qualified) <?php echo get_countstageapp($count_ansapp[0]['apps']);?></a></li>
					<li <?php echo $class;?>><a href="#pane<?php echo $roundCtr;?>-R" data-toggle="tab"><?php echo $tabName;?> (Review) <?php echo get_countapp($count_applicants[0]['apps']);?></a></li>
				<?php
					}	
				}
				?>
				<li><a href="#pane-y" data-toggle="tab">For Interview <?php echo get_countapp($interview_countpos);?></a></li>
				<li><a href="#pane-z" data-toggle="tab">Unsuccessful <?php echo get_countrejectapp($unsuccessful_countpos);?></a></li>
			  </ul>
			  <br/>
			  <div class="tab-content">
				<?php
				for($roundCtr = 1; $roundCtr <= $rounds; $roundCtr++){
					$status = "";
					$class = "";
					if($roundCtr == 1){$status = "New Applicant";$class="active";}
					$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid  AND applicant_status = '$status'";
					$applicants = $database->query($query);
					if($roundCtr == 1) { ?>
					<div id="pane<?php echo $roundCtr;?>" class="tab-pane <?php echo $class;?>">
						<?php print_newapplicantTable($applicants,$reference);?>
					</div>
				<?php }
					else {
					$status = "For Stage $roundCtr";
					$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid  AND applicant_status = '$status'";
					$applicants = $database->query($query);
				?>
					<div id="pane<?php echo $roundCtr;?>" class="tab-pane <?php echo $class;?>">
						<?php print_stageTable($applicants,$reference);?>
					</div>
				<?php 
					$status = "For Stage $roundCtr Review";
					$query = "SELECT * FROM navy_applicant WHERE applicant_position = $pid  AND applicant_status = '$status'";
					$applicants = $database->query($query);?>
					<div id="pane<?php echo $roundCtr;?>-R" class="tab-pane <?php echo $class;?>">
						<?php print_applicantTable($applicants,$reference);?>
					</div>
				<?php	
					}
				}
				?>
				<div id="pane-y" class="tab-pane">
					<?php print_applicantTable($interview_applicants,$reference);?>
				</div>
				<div id="pane-z" class="tab-pane">
				  <?php print_rejectTable($unsuccessful_applicants,$reference);?>
				</div>
			  </div><!-- /.tab-content -->
			</div><!-- /.tabbable -->
        </div>

<?php require_once("footer.php");?>
<?php require_once("pos-modal.php");?>
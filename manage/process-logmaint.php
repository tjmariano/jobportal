<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");
require_once("management-functions.php");

if(isset($_POST['readlogid'])){
	$log_id = $_POST['readlogid'];

	$log_obj = array(
			'log_read' => 1
		);
	$log_filter[] = array('log_id=%d', $log_id);
	$database->updateRows('navy_logs', $log_obj, $log_filter);
}
?>
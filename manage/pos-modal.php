  </div>
</div>

<!-- Modal
    ================================================== -->

<!-- ADD USER MODAL START -->
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New User</h4>
            </div>

            <div class="modal-body">
                <form id="addUserForm" method="post" action="process-usermaint.php">
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="Last Name" name="userLname" />
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="First Name" name="userFname" />
                  </div>
                  <div class="form-group input-group full-width">
                    <?php 
                          $query = "SELECT * FROM navy_set_type WHERE set_postype = 2 AND set_type_active = 1";
                          $user_pos = $database->query($query);
                    ?>
                    <select name="userPos" onchange = "changeMe(this);">
                      <option value = "" disabled selected>---Select User Position---</option>
                      <?php foreach($user_pos AS $pos){ ?>
                      <option value = "<?php echo $pos['set_type_id'];?>"><?php echo $pos['set_type_desc'];?></option>
                      <?php }?>
                    </select>

                  </div>
                  <div class="form-group input-group full-width">
                    <select name="userLevel" onchange = "changeMe(this);">
                      <option value = "" disabled selected>---Select User Level---</option>
                      <option value = "admin">admin</option>
                      <option value = "employee">employee</option>
                    </select>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="email" class="form-control" placeholder="E-mail Address" name="userEmail" />
                  </div>
                  <div class="form-group input-group">
                      <label for = "userSetmail">Do you want this user to receive e-mail notifications?<br/>
                        Note: You can change this in user maintenance.</label>
                    <div class = "radio">
                      <label>
                      <input type="radio" name="userSetmail" value = "1">YES
                      </label>
                    </div>
                    <div class = "radio">
                      <label>
                      <input type="radio" name="userSetmail" value = "0">NO
                      </label>
                    </div>
                  </div>
                  <div class="form-group">
                      <br/>
                        <button type="submit" name="submit_adduser" class="btn btn-default center-block" onclick="return confirm('Do you want to add the user?')">Submit</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- ADD USER MODAL END -->

<!-- EDIT USER MODAL START -->
    <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit <p id = "userfullname" class = "straight"></p> Account Details</h4>
            </div>

            <div class="modal-body">
                <form id="addUserForm" method="post" action="process-usermaint.php">
                  <div class="form-group input-group full-width">
                    <input type="hidden" name="edituserid" id="edituserid" />
                    <input type="text" class="form-control" placeholder="Last Name" name="edituserLname" id="edituserLname" />
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="First Name" name="edituserFname" id="edituserFname" />
                  </div>
                  <div class="form-group input-group full-width">
                    <?php 
                          $query = "SELECT * FROM navy_set_type WHERE set_postype = 2 AND set_type_active = 1";
                          $user_pos = $database->query($query);
                    ?>
                    <select name="edituserPos" id = "edituserPos" class = "form-control">
                      <option value = 'admin' style = "display:none;">admin</option>
                      <?php foreach($user_pos AS $pos){ ?>
                      <option value = "<?php echo $pos['set_type_id'];?>"><?php echo $pos['set_type_desc'];?></option>
                      <?php }?>
                    </select>

                  </div>
                  <div class="form-group input-group full-width">
                    <select name="edituserLevel" id = "edituserLevel" class = "form-control">
                      <option value = "admin">admin</option>
                      <option value = "employee">employee</option>
                    </select>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="email" class="form-control" placeholder="E-mail Address" name="edituserEmail" id="edituserEmail" />
                  </div>
                  <div class="form-group">
                      <br/>
                        <button type="submit" name="submit_edituser" class="btn btn-default" onclick="return confirm('Are you sure you want to update user account details?')">Submit</button>
                        <button id = "#btnresetpass" type = "submit" name="submit_resetuser" class="btn btn-info right" onclick="return confirm('Reset user password?')">Reset Password</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- EDIT USER MODAL END -->

<!-- USER CHANGE PASSWORD MODAL START -->
    <div class="modal fade" id="chgPassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>

            <div class="modal-body">
              <form id = "chgPassForm" method="post" action="process-usermaint.php">
                  <div class="form-group input-group full-width">
                    <input type="hidden" name="chg_userid" id = "chg_userid" />
                    <input type="hidden" name="chg_useruri" id = "chg_useruri" />
                    <input type="password" class="form-control" name="txtOldPass" id = "txtOldPass" placeholder = "Old Password" required/>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="password" class="form-control" name="txtNewPass" id = "txtNewPass" placeholder = "New Password" maxlength = "20" required pattern = ".{8,20}" title="Note: Password must at least be 8 to 20 characters"/>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="password" class="form-control" name="txtConfirmPass" id = "txtConfirmPass" placeholder = "Confirm New Password" maxlength = "20" required pattern = ".{8,20}" title="Note: Password must at least be 8 to 20 characters"/>
                  </div>
                  <div class="form-group">
                        <br/>
                          <button type="submit" name="submit_chgPassword" class="btn btn-default center-block" onclick="return confirm('Are you sure you want to change your password?')">Submit</button>
                      </div>
                </form>
                  
            </div>
          </div>
        </div>
      </div>
<!-- USER CHANGE PASSWORD MODAL END -->


<!-- USER ACCOUNT SETTINGS MODAL START -->
      <div class="modal fade" id="acctSetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Account Settings</h4>
            </div>

            <div class="modal-body">
              <form id = "acctSetForm" method="post" action="process-usermaint.php">
                  <div class="form-group input-group full-width">
                    <input type="hidden" name="acct_userid" id = "acct_userid" />
                    <input type="hidden" name="acct_useruri" id = "acct_useruri" />
                    <label for = "txtLname">Last Name:</label>
                    <input type="text" class="form-control" name="txtLname" id = "txtLname" readonly required/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "txtFname">First Name:</label>
                    <input type="text" class="form-control" name="txtFname" id = "txtFname" readonly required/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "txtPos">Position:</label>
                    <input type="text" class="form-control" name="txtPos" id = "txtPos" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "txtLevel">User Level:</label>
                    <input type="text" class="form-control" name="txtLevel" id = "txtLevel" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "txtEmail">Email Address:</label>
                    <input type="email" class="form-control" name="txtEmail" id = "txtEmail" placeholder = "E-mail Address" required title = "Bruuuhhh that's not a valid e-mail address."/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "setmail">Receive E-mail Notifications?</label>
                    <select name = "setmail" id = "setmail" class ="form-control">
                      <option value = "1">YES</option>
                      <option value = "0">NO</option>
                    </select>
                  </div>
                  <div class="form-group">
                        <br/>
                          <button type="submit" name="submit_acctset" class="btn btn-default center-block" onclick="return confirm('Are you sure you want to update your account settings?')">Submit</button>
                      </div>
                </form>
                  
            </div>
          </div>
        </div>
      </div>
<!-- USER ACCOUNT SETTINGS MODAL END -->

<!-- ADD POSITION MODAL START -->
    <div class="modal fade" id="addPosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New Position</h4>
            </div>

            <div class="modal-body">
                <form id="addPosForm" method="post" action="process-addpos.php">
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="Position Name" name="posName" />
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="Position Slug" name="posSlug" />
                  </div>
                  <div class="form-group input-group full-width">
                    <Select name = "posActive" onchange = "changeMe(this);">
                      <option value="" disabled selected>---Select Position Hiring Status---</option>
                      <option value = "0">Inactive</option>
                      <option value = "1">Active</option>
                    </Select>
                  </div>
                  <div class="form-group input-group full-width">
                    <Select name = "posType" onchange = "changeMe(this);">
                      <option value="" disabled selected>---Select Position Type---</option>
                      <option value = "1">Intern</option>
                      <option value = "2">Regular</option>
                    </Select>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="number" name ="posRounds" min = "1" max = "3" placeholder = "Specify Rounds" onkeypress = "return isNumber(event)" style = "width:50%;" required/>
                  </div>
                  <div class="form-group">
                      <br/>
                        <button type="submit" name="submit_answers" class="btn btn-default center-block" onclick="return confirm('Do you want to add the position?')">Submit</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- ADD POSITION MODAL END -->

<!-- EDIT POSITION MODAL START -->
      <div class="modal fade" id="editPosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit <p class = "straight" id = "posinfo"></p> Position</h4>
            </div>

            <div class="modal-body">
                <form id="editPosForm" method="post" action="process-updatepos.php">
                  <div class="form-group input-group full-width">
                    <input type = "hidden" name="editposid" id = "editposid" />
                    <input type="text" class="form-control" placeholder="Position Name" name="editposName" id = "editposName" />
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="text" class="form-control" placeholder="Position Slug" name="editposSlug" id ="editposSlug" />
                  </div>
                  <div class="form-group input-group full-width">
                    <Select name = "editposType" id = "editposType" class="form-control">
                      <option value = "1">Intern</option>
                      <option value = "2">Regular</option>
                    </Select>
                  </div>
                  <div class="form-group input-group full-width">
                    <input type="number" name ="editposRounds" id ="editposRounds" min = "1" max = "3" placeholder = "Specify Rounds" onkeypress = "return isNumber(event)" style = "width:50%;" required/>
                  </div>
                  <div class="form-group">
                      <br/>
                        <button type="submit" name="submit_editpos" class="btn btn-default center-block" onclick="return confirm('Are you sure you want to update the position?')">Submit</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- EDIT POSITION MODAL END -->

<!-- REJECT APPLICANT MODAL START -->
    <div class="modal fade" id="rejectAppModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Reject Applicant</h4>
            </div>
            <div class="modal-body">
                <form id="rejectAppForm" method="post" action="process.php">
                  <div class="form-group input-group full-width">
                    <input type = "hidden" name= "id" id = "rejectappid" />
                    <input type='hidden' name='ref' id = "rejectappref"/>
                    <label for = "rejectAppName">Applicant Name:</label>
                    <input type="text" class="form-control" name="rejectAppName" id = "rejectAppName" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    
                    <label for = "rejectAppPos">Position Applied:</label>
                    <input type="text" class="form-control" name="rejectAppPos" id = "rejectAppPos" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    
                    <label for = "rejectAppStat">Applicant Status:</label>
                    <input type="text" class="form-control" name="rejectAppStat" id = "rejectAppStat" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                  
                    <label for = "rejectcomment">Insert Reason for Rejection below (optional):</label>
                    <textarea name = "rejectcomment" class="form-control inputcomment"></textarea>
                  </div>
                  <div class="form-group">
                      <br/>
                        <button type="submit" name="rejected" class="btn btn-danger center-block" onclick="return confirm('Are you sure you want to reject this applicant?')">Reject Applicant</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- REJECT APPLICANT MODAL END -->

<!-- ACCEPT APPLICANT MODAL START -->
    <div class="modal fade" id="acceptAppModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Pass Applicant</h4>
            </div>

            <div class="modal-body">
                <form id="acceptAppForm" method="post" action="process.php">
                  <div class="form-group input-group full-width">
                    <input type = "hidden" name= "id" id = "acceptappid" />
                    <input type='hidden' name='ref' id = "acceptappref"/>
                    <label for = "acceptAppName">Applicant Name:</label>
                    <input type="text" class="form-control" name="acceptAppName" id = "acceptAppName" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    
                    <label for = "acceptAppPos">Position Applied:</label>
                    <input type="text" class="form-control" name="acceptAppPos" id = "acceptAppPos" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    
                    <label for = "acceptAppStat">Applicant Status:</label>
                    <input type="text" class="form-control" name="acceptAppStat" id = "acceptAppStat" readonly/>
                  </div>
                  <div id = "rate_star" class="form-group input-group full-width">
                    <label for = "starrystar">Please Rate Applicant(1-5, 5 as highest, 1 as lowest):</label>
                    <br/>
                    <span id = "starrystar" class="star-rating">
                      <input id = "the_rating" type="radio" name="rating" class = "radiorate" value="1" required title = "Stars are required. Oh so shiny stars!"><i></i>
                      <input type="radio" name="rating" class = "radiorate" value="2"><i></i>
                      <input type="radio" name="rating" class = "radiorate" value="3"><i></i>
                      <input type="radio" name="rating" class = "radiorate" value="4"><i></i>
                      <input type="radio" name="rating" class = "radiorate" value="5"><i></i>
                    </span>
                    <strong class="choice">Choose a rating</strong>
                  </div>
                  <div>
                      
                      <div id = "rndscore" class = "form-group input-group">
                        <fieldset>
                        <legend>Input Score (if applicable):</legend>
                        <label for = "testscore">Score:</label> 
                        <input name = "testscore" id = "testscore" type = "text" class="txtscore" onkeypress = "return isNumber(event)" maxlength = "3" title = "Score cannot be higher than overall."/>
                        <label for = "testoverall">Overall:</label> 
                        <input name = "testoverall" id = "testoverall" type = "text" class="txtscore" onkeypress = "return isNumber(event)" maxlength = "3"/>
                        </fieldset>
                      </div>
                      <div class="form-group input-group full-width">
                        <label id = "lblcomment" for = "acceptcomment"></label>
                        <textarea name = "acceptcomment" id = "acceptcomment" class="form-control inputcomment"></textarea>
                      </div>
                  </div>

                  <div class="form-group">
                      <br/>
                        <button type="submit" name="accepted" class="btn btn-success center-block" onclick="return confirm('Do you want the applicant to proceed to the next round?')">Pass Applicant</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- ACCEPT APPLICANT MODAL END -->

<!-- APPLICANT COMMENT MODAL START -->
    <div class="modal fade" id="appcommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id = "title-comment" class="modal-title"></h4>
            </div>

            <div class="modal-body">
                  <div class="form-group input-group full-width">
                    <label for = "commentAppName">Applicant Name:</label>
                    <input type="text" class="form-control" name="commentAppName" id = "commentAppName" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "commentAppPos">Position Applied:</label>
                    <input type="text" class="form-control" name="commentAppPos" id = "commentAppPos" readonly/>
                  </div>
                  <div class="form-group input-group full-width">
                    <label for = "commentAppStat">Applicant Status:</label>
                    <input type="text" class="form-control" name="commentAppStat" id = "commentAppStat" readonly/>
                  </div>
                
                  <div >
                    <label for = "appcomment">Review:</label>
                    <div class = "commentbox" id = "appcomment" readonly></div>
                  </div>
                  
            </div>
          </div>
        </div>
      </div>
<!-- APPLICANT COMMENT MODAL END -->

<!-- EDIT QUESTIONNAIRE MODAL START -->
    <div class="modal fade" id="editQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-questions">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit <p class = "straight" id = "pos-title"></p> Questionnaires</h4>
            </div>

            <div class="modal-body">
              
                <form id="questionsForm" method="post" action="process-updateQuestions.php">
                    <div class="form-group input-group full-width center">
                      <input type = "hidden" name = "qpos_id" id = "qpos_id" />
                      <select class = "roundctrl" onchange = "changeMe(this);" name = "qpos_round">
                        
                      </select>
                    </div>
                    <div>
                      <h1 id = "round_header" class = "center"></h1>
                      <table class='table table-responsive table-bordered'>
                        <thead>
                          <tr>
                            <th style ="width:5%">No.</th>
                            <th style ="width:85%">Question</th>
                            <th style ="width:10%">Type</th>
                          </tr>
                        </thead>
                        <tbody class = "questioncontainer">

                        </tbody>
                      </table>
                    </div>
                    <div id = "the_addbutton">

                    </div>
                    <div class="form-group">
                        <br/>
                          <button type="submit" name="submit_editQuestions" class="btn btn-default center-block" onclick="return confirm('Are you sure you want to update the questionnaire?')">Submit</button>
                      </div>
                </form>
            </div>
          </div>
        </div>
      </div>
<!-- EDIT QUESTIONNAIRE MODAL END -->

<!-- SHOW LOGS MODAL START -->
    <div class="modal fade" id="showLogsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-logs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Logs</h4>
            </div>

            <div class="modal-body">
                      <table class='table table-responsive table-bordered tblLogs'>
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Action</th>
                            <th>Date</th>
                            <th>Type</th>
                          </tr>
                        </thead>
                        <tbody class = "logscontainer">
                          <?php 
                            $query = "SELECT * FROM navy_logs WHERE log_company = 1";
                            $log_data = $database->query($query);
                            foreach($log_data as $log){ 
                              $query = "SELECT * FROM navy_users WHERE user_company = 1 and user_id = ".$log['log_user'];
                              $get_user = $database->query($query);
                              $fullname = $get_user[0]['user_fname']." ".$get_user[0]['user_lname'];?>
                            <tr>
                              <td><?php echo $fullname?></td>
                              <td><?php $obj_log = array($log['log_type'], $fullname, $log['log_action'], $log['log_target']);
                                    get_logtype($obj_log);?></td>
                              <td><?php echo $log['log_date'];?></td>
                              <td><?php echo $log['log_type'];?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
            </div>
          </div>
        </div>
      </div>
<!-- SHOW LOGS MODAL END -->
      
<!-- Modal END
    ================================================== -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/js/js-functions.js"></script>
    <script src="../bootstrap/formvalidation/js/formValidation.min.js"></script>
    <script src="../bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
    <script src="../bootstrap/js/jquery.circliful.min.js"></script>
    <script src="../bootstrap/js/jquery.datatables.min.js"></script>
    <script src="../bootstrap/js/datatables.js"></script>
    <script src="../ckeditor/ckeditor.js"></script>

    <script>
    function changeMe(sel)
    {
      sel.style.color = "#000";              
    }
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
    </script>

  </body>
<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("app_header.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title><?php echo $positionDescription;?> Application - FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	
	<link rel="stylesheet" href="/bootstrap/formvalidation/css/formValidation.min.css">
    <link href="/bootstrap/css/jumbotron.css" rel="stylesheet">
    <style>
      .1tab{display: none;}
    </style>
  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="internheader">
      <div class="container">
        <?php echo $the_header;?>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns --><p class = "whitespace"></p>
      	<h2 class="centered" style="font-size:30px;">You applied as a <?php echo $positionDescription;?>.</h2><br>
            <?php echo $the_button;?>
       </div>
   

      <hr>

     <?php 
require_once("$root/footer-front.php");
      ?>

    <!-- Modal
    ================================================== -->

<div class="modal fade" id="examModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $the_exam_title;?></h4>
            </div>

            <div class="modal-body">
              <?php echo $the_exam_content;?>
                <form id="marketingInternForm" method="post" action="processor.php" <?php add_uploadEncType($the_set_attachment);?>>
					<input type="hidden" name="app_token" value="<?php echo $token;?>"/>
					<input type="hidden" name="set_code" value="<?php echo $the_set_code;?>"/>
					<input type="hidden" name="set_order" value="<?php echo $the_set_order;?>"/>
					<input type="hidden" name="company_id" value = "<?php echo $companyID;?>"/>
					<input type="hidden" name="seta_type" value = "<?php echo $positionID;?>"/>
					<input type="hidden" name="position_description" value = "<?php echo $positionDescription;?>"/>
                   
				   
				   
                  <hr>
                  <?php
                    $questions = json_decode($set_data[0]['set_content'],true);
                    foreach($questions as $question){
						supply_questionFields($question,$the_set_code);
                    }
                  ?>


                    <div class="form-group">
                        <button type="submit" name="submit_application" class="btn btn-default center-block" style = "width: 50%" onclick="return confirm('Are you sure you want to submit?')">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
	<script src="/bootstrap/formvalidation/js/formValidation.min.js"></script>
	<script src="/bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
  </body>
</html>

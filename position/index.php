<?php 
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("header.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.fourptzero.com/favicon.ico">

    <title><?php echo $positionDescription;?> Application - FourPoint.Zero Inc.</title>

    <!-- Bootstrap core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	
	<link rel="stylesheet" href="/bootstrap/formvalidation/css/formValidation.min.css">
    <link href="/bootstrap/css/jumbotron.css" rel="stylesheet">

  </head>

  <body>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="internheader">
      <div class="container">
        <h1 class="centered" style="color:#fff;font-size: 60px;">So you wanna join the team.</h1><br>
        <p class="centered" style="color:#fff;font-size:20px;">Part of our core value is efficiency in everything we do. This includes how we add new members to the team.<br/>
        We built this tool to help us efficiently filter the winners from the posers. Shall we start?</p>
      </div>
    </div>

    <div class="container">
        <p class = "whitespace"></p>
      	<h2 class="centered" style="font-size:30px;">You applied as a <?php echo $positionDescription;?>.</h2><br>
      	<p class = "centered" style="font-size:20px;"><a class="btn btn-default samesize2" href="#" role="button" data-toggle="modal"  data-target="#startModal">Who we are. What we need.</a></p>
            <center><p><a class="btn btn-default samesize2" href="#" role="button" data-toggle="modal"  data-target="#examModal">Let's Start &raquo;</a></p></center>
       </div>
   

      <?php 
require_once("$root/footer-front.php");
      ?>

    <!-- Modal
    ================================================== -->
    <div class="modal fade" id="startModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class = "tabbable">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo $the_intro_title;?></a></li>
                <li><a href="#tab2" data-toggle="tab"><?php echo $the_requirement_title;?></a></li>
              </ul>
          <div class = "tab-content">
          <div class="tab-pane active" id="tab1">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $the_intro_title;?></h4>
            </div>

            <div class="modal-body">
				<?php echo $the_intro_content;?>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $the_requirement_title;?></h4>
            </div>

            <div class="modal-body">
				<?php echo $the_requirement_content;?>
        <center><p><a class="btn btn-default samesize" href="#" role="button" data-toggle="modal" id = "btnShowExam" data-target="#examModal">Let's Start &raquo;</a></p></center>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="examModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo $positionDescription;?> Initial Questionnaire</h4>
            </div>

            <div class="modal-body">
                <form id="applicationForm" method="post" action="processor.php" enctype="multipart/form-data">
					<input type="hidden" name="set_code" value="<?php echo $the_set_code;?>"/>
					<input type="hidden" name="set_order" value="<?php echo $the_set_order;?>"/>
					<input type="hidden" name="company_id" value = "<?php echo $companyID;?>"/>
					<input type="hidden" name="seta_type" value = "<?php echo $positionID;?>"/>
					<input type="hidden" name="position_description" value = "<?php echo $positionDescription;?>"/>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="First Name" name="firstName">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Last Name" name="lastName">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="email" class="form-control" placeholder="Email Address" name="email">
                    </div>
                    <div class="form-group input-group full-width">
                      <input type="text" class="form-control" placeholder="Contact Number" name="contactNumber" onkeypress = "return isNumber(event)">
                    </div>
                    <div class="form-group input-group full-width">
                      <p class="text-muted">Resume(PDF, DOC, DOCX)</p> 
                      <input type="file" class="form-control" name="file" id="file" accept = "application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                    </div>
                  <hr>
                  <p class = "justified"><b>Try to answer as many as you can.</b></p>
                  <?php
                    $questions = json_decode($set_data[0]['set_content'],true);
                    foreach($questions as $question){
						supply_questionFields($question,$the_set_code);
                    }
                    ?>


                    <div class="form-group">
                        <button type="submit" name="submit_answers" class="btn btn-default center-block" style = "width: 50%" onclick="return confirm('Are you sure you want to submit?')">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
	<script src="/bootstrap/formvalidation/js/formValidation.min.js"></script>
	<script src="/bootstrap/formvalidation/js/framework/bootstrap.min.js"></script>
	<script type = text/javascript>
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
  </script>
  <script type = text/javascript>
  $('#btnShowExam').on('click', function () {
  // Load up a new modal...
  $('#startModal').modal('hide')
  })
    $('#startModal').on('hidden', function () {
  // Load up a new modal...
  $('#examModal').modal('show')
  })
  </script>
	<script>
		$(document).ready(function() {
			$('#applicationForm').formValidation({
				framework: 'bootstrap',
				icon: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					firstName: {
						validators: {
							notEmpty: {
								message: 'First Name is required'
							}
						}
					},
					lastName: {
						validators: {
							notEmpty: {
								message: 'Last Name is required'
							}
						}
					},
					email: {
						validators: {
							notEmpty: {
								message: 'Email Address is required'
							},
							emailAddress: {
								message: 'Bruuuhhh that\'s not a valid e-mail address.'
							}
						}
					},
					contactNumber: {
						validators: {
							notEmpty: {
								message: 'Contact Number is required'
							}
						}
					},
					file: {
						validators: {
							notEmpty: {
								message: 'Resume is required'
							}
						}
					}
				}
			});
		});
	</script>
	
  </body>
</html>

<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/ignition.php");

$loc = get_home();
$curr_date = date('Y-m-d H:i:s');

if(isset($_POST['submit_answers'])){
	$applicant_token = md5($_POST['email']."-".$curr_date);

	$query = "SELECT * FROM navy_applicant WHERE applicant_email = '".$_POST['email']."'";
	$app_data = $database->query($query);

	$count_app = count($app_data);

	$resume = $_FILES["file"]["name"];
	$ext = pathinfo($resume, PATHINFO_EXTENSION);	

	$applicant_object = 
		array(
			'applicant_token' 		=>	$applicant_token,
			'applicant_first_name' 	=>	$_POST['firstName'],
			'applicant_last_name' 	=>	$_POST['lastName'],
			'applicant_email'	 	=>	$_POST['email'],
			'applicant_contact' 	=>	$_POST['contactNumber'],
			'applicant_company' 	=>	$_POST['company_id'],
			'applicant_position'	=>	$_POST['seta_type'],
			'applicant_phase'	 	=>	$_POST['set_order'],
			'applicant_resume'		=>	$ext,
			'applicant_read'		=>	0

		);
	if($count_app == 1){
		$loc = get_errorPage();
	}

	else{
		if($database->insertRow('navy_applicant', $applicant_object)){
			$query = "SELECT applicant_id FROM navy_applicant WHERE applicant_token = '$applicant_token'";
			$data = $database->query($query);

			/*Upload Resume File*/
			$allowedExts = array("pdf","doc","docx");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if (($_FILES["file"]["type"] == "application/pdf" || $_FILES["file"]["type"] == "application/msword" || $_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")  &&($_FILES["file"]["size"] < 2000000) &&in_array($extension, $allowedExts)){
				if ($_FILES["file"]["error"] > 0){
					echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
				}else{
					move_uploaded_file($_FILES["file"]["tmp_name"], "$root/uploads/" .$applicant_token.".$extension");
				}
			}
			
			/*Insert Answers*/
			$query = "SELECT set_content FROM navy_set WHERE set_company = ".$_POST['company_id']." AND set_code = '".$_POST['set_code']."'";
			$set_data = $database->query($query);
			
			$answers = array();
			$questions = json_decode($set_data[0]['set_content'],true);
			foreach($questions as $question){
				$question_id = $_POST['set_code'].'-'.$question['order'];
				$temp = array($question['order'],$_POST[$question_id]);
				array_push($answers,$temp);
			}
			
			$answer_object = array(
				'ans_applicant'		=> $data[0]['applicant_id'],
				'ans_set'			=> $_POST['set_code'],
				'ans_answer'		=> json_encode($answers),
				'ans_date_taken'	=> $curr_date
			);


				if($database->insertRow('navy_answer', $answer_object)){
				$query = "SELECT * FROM navy_users WHERE user_setmail = 1 AND user_company = 1 AND user_active = 1";
				$user_data = $database->query($query);


				$mail_object1 = array($_POST['email'],$_POST['position_description'],$_POST['firstName']);
				send_applicantNotification($mail_object1);

				foreach($user_data as $user){
					$userfullname = $user['user_lname'].", ".$user['user_fname'];
					$mail_object2 = array($_POST['position_description'],$_POST['set_order'],$_POST['firstName'],$_POST['lastName'], $user['user_email'], $userfullname);
					send_adminNotification($mail_object2);
				}
				
				
				$loc = get_thankYouPage();
				}else{
					//error
					$loc = get_errorPage();
				}
			

			

		}else{
			//error
			$loc = get_errorPage();
		}
	
	}
}


if(isset($_POST['submit_application'])){
	$query = "SELECT * FROM navy_applicant WHERE applicant_token = '".$_POST['app_token']."'";
	$data = $database->query($query);

/*	if($_data[0]['applicant_phase'] != $order){
	goToError();
	}
	elseif($_data[0]['applicant_position'] != $position){
		goToError();
	}
	elseif($_data[0]['applicant_status'] == "For Stage ". $app_data[0]['applicant_phase'] ." Review"){
		goToError();
	}*/

	$query = "SELECT * FROM navy_set_type WHERE set_type_id = ".$data[0]['applicant_position']."";
	$pos_data = $database->query($query);
		
	$query = "SELECT set_postype_id FROM navy_set_postype WHERE set_postype_id = ".$pos_data[0]['set_postype']."";
	$postype_data = $database->query($query);
		
	/*Insert Answers*/
	$query = "SELECT set_content FROM navy_set WHERE set_company = ".$_POST['company_id']." AND set_code = '".$_POST['set_code']."'";
	$set_data = $database->query($query);
	
	$answers = $uploadables = $images = array();
	$questions = json_decode($set_data[0]['set_content'],true);
	foreach($questions as $question){
		$question_id = $_POST['set_code'].'-'.$question['order'];
		if($question['type'] === 'file'){
			$fileans = $_FILES["$question_id"]["name"];
			$exten = pathinfo($fileans, PATHINFO_EXTENSION);
			$temp = array($question['order'],$data[0]['applicant_id']."-$question_id.".$exten);
			array_push($uploadables,$question_id);
		}
		elseif($question['type'] === 'image'){
			$temp = array($question['order'],$data[0]['applicant_id']."-$question_id.png");
			array_push($uploadables,$question_id);
		}
		else{
			$temp = array($question['order'],$_POST[$question_id]);
		}
		array_push($answers,$temp);
	}
	
	if(!empty($uploadables)){
		foreach($uploadables as $uploadable){
			$allowedExts = array("pdf","PDF", "png", "PNG","doc","docx");
			$temp = explode(".", $_FILES["$uploadable"]["name"]);
			$extension = end($temp);
			if (($_FILES["$uploadable"]["type"] == "application/pdf" || $_FILES["$uploadable"]["type"] == "image/png" || $_FILES["$uploadable"]["type"] == "application/msword" || $_FILES["$uploadable"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")&&($_FILES["$uploadable"]["size"] < 5000000) &&in_array($extension, $allowedExts)){
				if ($_FILES["$uploadable"]["error"] > 0){
					echo "Return Code: " . $_FILES["$uploadable"]["error"] . "<br>";
				}else{
					move_uploaded_file($_FILES["$uploadable"]["tmp_name"], "$root/uploads/" .$data[0]['applicant_id']."-$uploadable.$extension");

				}
			}
		}
	}
	
	$answer_object = array(
		'ans_applicant'		=> $data[0]['applicant_id'],
		'ans_set'			=> $_POST['set_code'],
		'ans_answer'		=> json_encode($answers),
		'ans_date_taken'	=> $curr_date
	);

	$query = "SELECT * FROM navy_answer WHERE ans_applicant = ".$data[0]['applicant_id']." AND ans_set = '".$_POST['set_code']."'";
	$check_ans = $database->query($query);

	$count_checkans = count($check_ans);

	if($count_checkans == 1){
		$loc = get_errorPage();
	}	
	else{
			if($database->insertRow('navy_answer', $answer_object)){
			$query = "SELECT * FROM navy_users WHERE user_setmail = 1 AND user_company = 1 AND user_active = 1";
				$user_data = $database->query($query);
				

			$applicant_obj = array(
							'applicant_status'	=> $data[0]['applicant_status']." Review",
							'applicant_read'	=> 0
							);
		
			$applicant_filter[] = array('applicant_token=%s', $_POST['app_token']);
			$database->updateRows('navy_applicant', $applicant_obj, $applicant_filter);
		
			$mail_object1 = array($data[0]['applicant_email'],$_POST['position_description'],$data[0]['applicant_first_name']);
			send_applicantNotificationUpdate($mail_object1);
			
			foreach($user_data as $user){
					$userfullname = $user['user_lname'].", ".$user['user_fname'];
					$mail_object2 = array($_POST['position_description'],$_POST['set_order'],$data[0]['applicant_first_name'],$data[0]['applicant_last_name'], $user['user_email'], $userfullname);
					send_adminNotification($mail_object2);
				}
			
			
			if($postype_data[0]['set_postype_id'] === '1'){

					$loc = get_thankYou3Page();
			}
			elseif($postype_data[0]['set_postype_id'] === '2'){
				if($_POST['set_order'] === '2'){
					$loc = get_thankYou2Page();
				}
				elseif($_POST['set_order'] === '3'){
					$loc = get_thankYou3Page();
				}
			}
			
		}else{
			//error
			$loc = get_errorPage();
		}
	}
	
}

header("Location: $loc");
